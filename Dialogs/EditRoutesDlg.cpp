﻿#include "EditRoutesDlg.h"

#include <qmessagebox.h>
#include <qbuttongroup.h>
#include <qcombobox.h>
#include "../Scenes/RoutesScene.h"
#include "../SignalSenders/FocusSignalSender.h"
#include "../GraphicsItems/RouteGraphicsItem.h"

EditRoutesDlg::EditRoutesDlg(uint spaceWidth, uint spaceHeight, std::vector<RouteInfo>& RouteInfos, QWidget *parent)
	: QDialog(parent),
	scene_(new RoutesScene(this)),
	pointerTypeGroup_(this),
	spaceWidth_(500),
	spaceHeight_(500),
	noiseLevel_(-20),
	selectedRoute_(nullptr){
	ui_.setupUi(this);

	ui_.edtSpaceWidth_->setText(QString::number(spaceWidth_));
	ui_.edtSpaceHeight_->setText(QString::number(spaceHeight_));
	ui_.edtNoiseLevel_->setText(QString::number(noiseLevel_));

	scene_->setBackgroundBrush(QBrush(Qt::black));
	scene_->setSpaceBounds(spaceWidth_, spaceHeight_);
	scene_->setMode(RoutesScene::Mode::MOVE_ROUTE);
	ui_.view_->setScene(scene_);
	//ui_.view_->setRenderHint(QPainter::RenderHint::Antialiasing, true);

	ui_.tblObjectPos_->setRowCount(5);
	ui_.tblObjectPos_->setColumnCount(2);
	ui_.tblObjectPos_->setHorizontalHeaderLabels({"x","y"});
	ui_.tblObjectPos_->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Stretch);
	ui_.tblObjectPos_->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Stretch);
	
	QObject::connect(ui_.btnAcceptSpaceOptions_, &QPushButton::released, this, &EditRoutesDlg::onAcceptSpaceOptions);
	QObject::connect(new FocusSignalSender(ui_.edtObjectSignal_), &FocusSignalSender::focusChanged, this, &EditRoutesDlg::onNewObjectSignalValue);
	QObject::connect(scene_, &RoutesScene::routeInserted, [this](const RouteGraphicsItem* routeItem) {
		displayRouteData(routeItem);
		routeSignalValues_.push_back(std::make_pair(routeItem, 0.f));
		selectedRoute_ = routeItem;
		//set ui controls
		pointerTypeGroup_.button(1)->setChecked(true);//button move routes
		scene_->setMode(RoutesScene::Mode::MOVE_ROUTE);
		pointerTypeGroup_.button(0)->setChecked(false);//button insert routes
	});
	QObject::connect(scene_, &QGraphicsScene::selectionChanged, [this]() {
		QList<QGraphicsItem*> list = scene_->selectedItems();
		if (list.count() > 0) {
			RouteGraphicsItem* item = qgraphicsitem_cast<RouteGraphicsItem*>(list.first());
			displayRouteData(item);
			selectedRoute_ = item;
		}
	});
	QObject::connect(scene_, &RoutesScene::routeReleased, [this](const RouteGraphicsItem* routeItem) {
		displayRouteData(routeItem);
	});
	QObject::connect(scene_, &RoutesScene::routeUnselected, [this]() {
		//clear route data
		for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
			//x pos
			auto* xPos = new QTableWidgetItem("");
			xPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
			ui_.tblObjectPos_->setItem(i, 0, xPos);
			//y pos
			auto* yPos = new QTableWidgetItem("");
			yPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
			ui_.tblObjectPos_->setItem(i, 1, yPos);
		}
		//velocity and signal
		ui_.lblXVelocityValue_->setText("");
		ui_.lblYVelocityValue_->setText("");
		ui_.lblVelocityValue_->setText("");
		ui_.edtObjectSignal_->setText("");
	});
	QObject::connect(&pointerTypeGroup_, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), [this](int id) {
		if (id == 0)
			scene_->setMode(RoutesScene::Mode::INSERT_ROUTE);
		else if (id == 1)
			scene_->setMode(RoutesScene::Mode::MOVE_ROUTE);
	});
	QObject::connect(ui_.cmbZoom_, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index) {
		double newScale = index + 1;
		QMatrix oldMatrix = ui_.view_->matrix();
		ui_.view_->resetMatrix();
		ui_.view_->translate(oldMatrix.dx(), oldMatrix.dy());
		ui_.view_->scale(newScale, newScale);
	});

	//setup tool buttons
	ui_.btnInsertRoutes_->setCheckable(true);
	ui_.btnInsertRoutes_->setIcon(QIcon(":/res/img/linepointer.png"));
	ui_.btnInsertRoutes_->setToolTip("Wstaw trasę");
	ui_.btnMoveRoutes_->setCheckable(true);
	ui_.btnMoveRoutes_->setChecked(true);
	ui_.btnMoveRoutes_->setIcon(QIcon(":/res/img/pointer.png"));
	ui_.btnMoveRoutes_->setToolTip("Przesuwaj trasy");
	pointerTypeGroup_.addButton(ui_.btnInsertRoutes_, 0);
	pointerTypeGroup_.addButton(ui_.btnMoveRoutes_, 1);

	//add routes
	for (const RouteInfo& route : RouteInfos) {
		RouteGraphicsItem* item = new RouteGraphicsItem();
		for (int i = 0; i < NUMBER_OF_SPACES - 1; ++i) {
			//y coord needs to be converted
			item->setPoint(i, QPointF(route.x_[i + 1] - route.x_[0], route.y_[0] - route.y_[i + 1]));
		}
		item->setColor(Qt::green);
		item->setPos(route.x_[0], spaceWidth_ - 1 - route.y_[0]);
		item->setFlag(QGraphicsItem::ItemIsMovable, true);
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
		item->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
		scene_->addItem(item);
		routeSignalValues_.push_back(std::make_pair(item, route.signal_));
	}

	//zoom
	QStringList scales;
	scales << tr("1x") << tr("2x") << tr("3x") << tr("4x") << tr("5x") << tr("6x") << tr("7x") << tr("8x") << tr("9x") << tr("10x");
	ui_.cmbZoom_->addItems(scales);
}

EditRoutesDlg::~EditRoutesDlg(){
}

uint EditRoutesDlg::getSpaceWidth() const{
	return spaceWidth_;
}

uint EditRoutesDlg::getSpaceHeight() const{
	return spaceHeight_;
}

float EditRoutesDlg::getNoiseLevel() const{
	return noiseLevel_;
}

void EditRoutesDlg::getRoutes(std::vector<RouteInfo>& routes) const{
	for (const auto& pair : routeSignalValues_) {
		const QPointF first = pair.first->pos();
		const QPointF second = pair.first->getPoint(NUMBER_OF_SPACES-2);
		//y coords needs to be converted
		int x1 = static_cast<int>(first.x());
		int y1 = spaceHeight_ - 1 - static_cast<int>(first.y());
		int x2 = static_cast<int>(first.x()+second.x());
		int y2 = spaceHeight_ - 1 - static_cast<int>(first.y() + second.y());
		float signalValue = pair.second;
		routes.push_back(RouteInfo(x1, y1, x2, y2, signalValue));
	}
}

void EditRoutesDlg::onAcceptSpaceOptions() {
	//validate user input
	bool ok = false;
	uint spaceWidth = ui_.edtSpaceWidth_->text().toUInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość szerokości przestrzeni!");
		return;
	}
	uint spaceHeight = ui_.edtSpaceHeight_->text().toUInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość wysokości przestrzeni!");
		return;
	}
	float noiseLevel = ui_.edtNoiseLevel_->text().toFloat(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość poziomu szumu!");
		return;
	}
	//save new options
	spaceWidth_ = spaceWidth;
	spaceHeight_ = spaceHeight;
	noiseLevel_ = noiseLevel;
	//notify scene
	scene_->setSpaceBounds(spaceWidth, spaceHeight);
	scene_->update();
}

void EditRoutesDlg::onNewObjectSignalValue(){
	auto it = std::find_if(routeSignalValues_.begin(), routeSignalValues_.end(), [this](const std::pair<const RouteGraphicsItem*, float>& pair) {
		return pair.first == selectedRoute_;
	});
	//validate user input
	bool ok = false;
	float signal = ui_.edtObjectSignal_->text().toFloat(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość sygnału!");
		ui_.edtObjectSignal_->setText(QString::number(it->second));
		return;
	}
	//set new value for object signal
	if (it != routeSignalValues_.end())
		it->second = signal;
}

void EditRoutesDlg::displayRouteData(const RouteGraphicsItem* object){
	//y coords need to be converted
	const qreal x1 = object->pos().x();
	const qreal y1 = spaceHeight_ - 1 - object->pos().y();
	const qreal x2 = object->getPoint(NUMBER_OF_SPACES - 2).x() + x1;
	const qreal y2 = y1 - object->getPoint(NUMBER_OF_SPACES - 2).y();
	const qreal dx = x2 - x1;
	const qreal dy = y2 - y1;
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		int x = static_cast<int>(x1 + i*dx / (NUMBER_OF_SPACES - 1));
		int y = static_cast<int>(y1 + i*dy / (NUMBER_OF_SPACES - 1));
		//x pos
		auto* xPos = new QTableWidgetItem(QString::number(x));
		xPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
		ui_.tblObjectPos_->setItem(i, 0, xPos);
		//y pos
		auto* yPos = new QTableWidgetItem(QString::number(y));
		yPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
		ui_.tblObjectPos_->setItem(i, 1, yPos);
	}
	//velocity and signal
	ui_.lblXVelocityValue_->setText(QString::number(dx / (NUMBER_OF_SPACES - 1)));
	ui_.lblYVelocityValue_->setText(QString::number(dy / (NUMBER_OF_SPACES - 1)));
	ui_.lblVelocityValue_->setText(QString::number(sqrt(dx*dx + dy*dy) / (NUMBER_OF_SPACES - 1)));
	auto it = std::find_if(routeSignalValues_.begin(), routeSignalValues_.end(), [this](const std::pair<const RouteGraphicsItem*, float>& pair) {
		return pair.first == selectedRoute_;
	});
	if (it != routeSignalValues_.end())
		ui_.edtObjectSignal_->setText(QString::number(it->second));
	else
		//its object that is being inserted so just put 0
		ui_.edtObjectSignal_->setText("0");
}
