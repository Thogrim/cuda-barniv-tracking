#include "ChooseDeviceDlg.h"

#include <QtWidgets/qlistwidget.h>
#include <cuda_runtime_api.h>

ChooseDeviceDlg::ChooseDeviceDlg(QWidget *parent)
	: QDialog(parent),
	selectedDevice_(-1),
	selectedDeviceName_(""){
	ui_.setupUi(this);
	QObject::connect(ui_.btnOk_, &QPushButton::released, this, [&]() {
		selectedDevice_ = ui_.lstAvailableDevices_->currentRow();
		if(selectedDevice_>=0)
			selectedDeviceName_ = ui_.lstAvailableDevices_->currentItem()->text();
		close();
	});
	QObject::connect(ui_.btnCancel_, &QPushButton::released, this, &QWidget::close);
	//displaying only one device for now
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, 0);
	new QListWidgetItem(prop.name, ui_.lstAvailableDevices_);
}

ChooseDeviceDlg::~ChooseDeviceDlg(){
}

int ChooseDeviceDlg::getSelectedDevice() const{
	return selectedDevice_;
}

const QString& ChooseDeviceDlg::getSelectedDeviceName() const{
	return selectedDeviceName_;
}