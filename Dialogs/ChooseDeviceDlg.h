#pragma once

#include <QDialog>
#include "ui_ChooseDeviceDlg.h"

class ChooseDeviceDlg : public QDialog{
	Q_OBJECT
public:
	ChooseDeviceDlg(QWidget *parent = Q_NULLPTR);
	~ChooseDeviceDlg();

	int getSelectedDevice() const;
	const QString& getSelectedDeviceName() const;

private:
	Ui::ChooseDeviceDlg ui_;
	int selectedDevice_;
	QString selectedDeviceName_;
};
