#pragma once

#include "ui_ShowResultsDlg.h"
#include <qdialog.h>
#include "../RouteInfo.h"

class RoutesScene;
class RouteGraphicsItem;

class ShowResultsDlg : public QDialog{
	Q_OBJECT

public:
	ShowResultsDlg(uint spaceWidth, uint spaceHeight, std::vector<RouteInfo>& inputRoutes, std::vector<RouteInfo>& resultRoutes, QWidget *parent = nullptr);
	~ShowResultsDlg();

private:
	void displayRouteData(const RouteGraphicsItem* object);

	Ui::ShowResultsDlg ui_;
	RoutesScene* scene_;
	uint spaceWidth_;
	uint spaceHeight_;
	std::vector<std::pair<RouteGraphicsItem*, float>> inputRouteSignalValues_;
	std::vector<std::pair<RouteGraphicsItem*, float>> resultRouteSignalValues_;
	const RouteGraphicsItem* selectedRoute_;
};
