#include "ShowResultsDlg.h"

//#include <qgraphicsitem.h>
#include <qcombobox.h>
#include "../Scenes/RoutesScene.h"
#include "../GraphicsItems/RouteGraphicsItem.h"

#define NUMBER_OF_SPACES 5

ShowResultsDlg::ShowResultsDlg(uint spaceWidth, uint spaceHeight, std::vector<RouteInfo>& inputRoutes, std::vector<RouteInfo>& resultRoutes, QWidget *parent)
	:QDialog(parent),
	scene_(new RoutesScene()),
	spaceWidth_(spaceWidth),
	spaceHeight_(spaceHeight),
	selectedRoute_(nullptr){
	ui_.setupUi(this);
	ui_.edtSpaceWidth_->setText(QString::number(spaceWidth_));
	ui_.edtSpaceHeight_->setText(QString::number(spaceHeight_));
	scene_->setBackgroundBrush(QBrush(Qt::black));
	scene_->setSpaceBounds(spaceWidth_, spaceHeight_);
	scene_->setMode(RoutesScene::Mode::MOVE_ROUTE);
	ui_.view_->setScene(scene_);

	ui_.tblObjectPos_->setRowCount(5);
	ui_.tblObjectPos_->setColumnCount(2);
	ui_.tblObjectPos_->setHorizontalHeaderLabels({ "x","y" });
	ui_.tblObjectPos_->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Stretch);
	ui_.tblObjectPos_->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Stretch);

	QObject::connect(scene_, &QGraphicsScene::selectionChanged, [this]() {
		QList<QGraphicsItem*> list = scene_->selectedItems();
		if (list.count() > 0) {
			RouteGraphicsItem* item = qgraphicsitem_cast<RouteGraphicsItem*>(list.first());
			displayRouteData(item);
			selectedRoute_ = item;
		}
	});
	QObject::connect(scene_, &RoutesScene::routeReleased, [this](const RouteGraphicsItem* routeItem) {
		displayRouteData(routeItem);
	});
	QObject::connect(scene_, &RoutesScene::routeUnselected, [this]() {
		//clear route data
		for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
			//x pos
			auto* xPos = new QTableWidgetItem("");
			xPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
			ui_.tblObjectPos_->setItem(i, 0, xPos);
			//y pos
			auto* yPos = new QTableWidgetItem("");
			yPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
			ui_.tblObjectPos_->setItem(i, 1, yPos);
		}
		//velocity and signal
		ui_.lblXVelocityValue_->setText("");
		ui_.lblYVelocityValue_->setText("");
		ui_.lblVelocityValue_->setText("");
	});
	QObject::connect(ui_.chkInputRoutes_, &QCheckBox::stateChanged, [this](int state) {
		if (state == Qt::CheckState::Unchecked) {
			//remove routes
			for (const auto& pair : inputRouteSignalValues_)
				scene_->removeItem(pair.first);
		}
		else if (state == Qt::CheckState::Checked) {
			//insert routes
			for (const auto& pair : inputRouteSignalValues_)
				scene_->addItem(pair.first);
		}
	});
	QObject::connect(ui_.chkResultRoutes_, &QCheckBox::stateChanged, [this](int state) {
		if (state == Qt::CheckState::Unchecked) {
			//remove routes
			for (const auto& pair : resultRouteSignalValues_)
				scene_->removeItem(pair.first);
		}
		else if (state == Qt::CheckState::Checked) {
			//insert routes
			for (const auto& pair : resultRouteSignalValues_)
				scene_->addItem(pair.first);
		}
	});
	QObject::connect(ui_.cmbZoom_, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index) {
		double newScale = index + 1;//scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
		QMatrix oldMatrix = ui_.view_->matrix();
		ui_.view_->resetMatrix();
		ui_.view_->translate(oldMatrix.dx(), oldMatrix.dy());
		ui_.view_->scale(newScale, newScale);
	});

	//zoom
	QStringList scales;
	scales << tr("1x") << tr("2x") << tr("3x") << tr("4x") << tr("5x") << tr("6x") << tr("7x") << tr("8x") << tr("9x") << tr("10x");
	ui_.cmbZoom_->addItems(scales);

	for (const RouteInfo& route : inputRoutes) {
		RouteGraphicsItem* item = new RouteGraphicsItem();
		for (int i = 0; i < NUMBER_OF_SPACES - 1; ++i) {
			//y coord conversion
			item->setPoint(i, QPointF(route.x_[i + 1] - route.x_[0], route.y_[0] - route.y_[i + 1]));
		}
		item->setColor(Qt::green);
		item->setPos(route.x_[0], spaceWidth_ - 1 - route.y_[0]);
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
		inputRouteSignalValues_.push_back(std::make_pair(item,route.signal_));
		scene_->addItem(item);
	}
	for (const RouteInfo& route : resultRoutes) {
		RouteGraphicsItem* item = new RouteGraphicsItem();
		for (int i = 0; i < NUMBER_OF_SPACES - 1; ++i) {
			//y coord conversion
			item->setPoint(i, QPointF(route.x_[i + 1] - route.x_[0], route.y_[0] - route.y_[i + 1]));
		}
		item->setColor(Qt::red);
		item->setPos(route.x_[0], spaceWidth_ - 1 - route.y_[0]);
		item->setFlag(QGraphicsItem::ItemIsSelectable, true);
		resultRouteSignalValues_.push_back(std::make_pair(item, route.signal_));
		scene_->addItem(item);
	}
}

ShowResultsDlg::~ShowResultsDlg(){
}

void ShowResultsDlg::displayRouteData(const RouteGraphicsItem* object){
	const qreal x1 = object->pos().x();
	const qreal y1 = spaceHeight_ - 1 - object->pos().y();
	const qreal x2 = object->getPoint(NUMBER_OF_SPACES - 2).x() + x1;
	const qreal y2 = y1 - object->getPoint(NUMBER_OF_SPACES - 2).y();;
	const qreal dx = x2 - x1;
	const qreal dy = y2 - y1;
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		int x = static_cast<int>(x1 + i*dx / (NUMBER_OF_SPACES - 1));
		int y = static_cast<int>(y1 + i*dy / (NUMBER_OF_SPACES - 1));
		//x pos
		auto* xPos = new QTableWidgetItem(QString::number(x));
		xPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
		ui_.tblObjectPos_->setItem(i, 0, xPos);
		//y pos
		auto* yPos = new QTableWidgetItem(QString::number(y));
		yPos->setTextAlignment(Qt::AlignmentFlag::AlignCenter);
		ui_.tblObjectPos_->setItem(i, 1, yPos);
	}
	//velocity and signal
	ui_.lblXVelocityValue_->setText(QString::number(dx / (NUMBER_OF_SPACES - 1)));
	ui_.lblYVelocityValue_->setText(QString::number(dy / (NUMBER_OF_SPACES - 1)));
	ui_.lblVelocityValue_->setText(QString::number(sqrt(dx*dx + dy*dy) / (NUMBER_OF_SPACES - 1)));
}

