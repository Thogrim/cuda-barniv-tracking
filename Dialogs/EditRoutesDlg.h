#pragma once

#include "ui_EditRoutesDlg.h"

#include <qdialog.h>
#include "../RouteInfo.h"

class RoutesScene;
class RouteGraphicsItem;

class EditRoutesDlg : public QDialog{
	Q_OBJECT

public:
	EditRoutesDlg(uint spaceWidth, uint spaceHeight, std::vector<RouteInfo>& RouteInfos, QWidget *parent = Q_NULLPTR);
	~EditRoutesDlg();

	uint getSpaceWidth() const;
	uint getSpaceHeight() const;
	float getNoiseLevel() const;
	void getRoutes(std::vector<RouteInfo>& routes) const;

private slots:
	void onAcceptSpaceOptions();

private:
	void onNewObjectSignalValue();
	//void displayObjectData(const RouteInfo& object);
	void displayRouteData(const RouteGraphicsItem* object);

	Ui::EditRoutesDlg ui_;
	RoutesScene* scene_;
	//QGraphicsView* view_;
	QButtonGroup pointerTypeGroup_;

	uint spaceWidth_;
	uint spaceHeight_;
	float noiseLevel_;
	std::vector<std::pair<const RouteGraphicsItem*,float>> routeSignalValues_;
	const RouteGraphicsItem* selectedRoute_;
};