#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_CudaTrackingQtApp.h"
//#include <vector>

#include "CudaTrackingDrawable.h"
#include "CudaTrackingBarnivAlgorithm.h"
#include "RouteInfo.h"

class CudaTrackingQtApp : public QMainWindow, public CudaTrackingDrawable{
    Q_OBJECT

public:
	CudaTrackingQtApp(QWidget *parent = Q_NULLPTR);
	~CudaTrackingQtApp();

public slots:
	void onBtnZoomIn();
	void onBtnZoomOut();
	void onNewGlWidgetSelection(int objectX, int objectY);
	void onChangeMinMaxSignalValues();
	void onChooseDevice();
	void onInitProgram();
	void onGenerateSignal();
	void onStartTracking();
	void onEditTracks();
	void onPlotMousePress(QMouseEvent* event);

public:
	virtual void draw(const DrawingState& drawState) const override;

private:
	void initRelativeStates();
	void resetProgram();
	void enableSpaceControls();
	void disableSpaceControls();
	void enableCostFunctionControls();
	void disableCostFunctionControls();

    Ui::CudaTrackingQtAppClass ui_;
	int chosenDevice_;	//id of chosen cuda device
	float* devSpaces_[NUMBER_OF_SPACES];
	float* hostSpaces_[NUMBER_OF_SPACES];
	float* hostOneStateCostFunction_;//values of cost function in one state
	CudaTrackingBarnivAlgorithm barniv_;

	int currentSpaceIndex_;
	int currentRelStateIndex_;
	int currentY_;
	bool rdbGeneratedSignalClicked_;
	std::vector<RouteInfo> trackedObjects_;
	std::vector<RouteInfo> resultObjects_;
};
