#pragma once

#include <vector>
#include <driver_types.h>

#include "CudaTrackingBarnivConstants.h"
#include "CudaTrackingBarnivTypes.h"

struct Route {
	uint x_[NUMBER_OF_SPACES];
	uint y_[NUMBER_OF_SPACES];
};

class CudaTrackingBarnivAlgorithm {
public:
	CudaTrackingBarnivAlgorithm();
	CudaTrackingBarnivAlgorithm(const CudaTrackingBarnivAlgorithm&) = delete;
	CudaTrackingBarnivAlgorithm(CudaTrackingBarnivAlgorithm&&) = delete;
	CudaTrackingBarnivAlgorithm& operator=(const CudaTrackingBarnivAlgorithm&) = delete;
	~CudaTrackingBarnivAlgorithm();
	cudaError setRelativeStatesOnDevice() const;
	size_t getRequiredMemorySize() const;
	const float* getDevCurrentCostFunction(const RelStateIndexValue relStateIndex) const;
	const float* getDevPreviousCostFunction(const RelStateIndexValue relStateIndex) const;
	const float* getDevMaxCostFunctionValues() const;
	const float* getHostMaxCostFunctionValues() const;
	const std::vector<Route>& getDetectedRoutes() const;
	cudaError initCudaTracking();
	void uninitializeCudaTracking();
	cudaError performBarnivDpaTracking(float* devSpaces[NUMBER_OF_SPACES]);

	//move to struct?
	uint spaceWidth_;
	uint spaceHeight_;
	RelStateValue* xRelStates_;
	RelStateValue* yRelStates_;
	RelStateIndexValue relStatesCount_;

private:	
	void freeDeviceMemory();
	void extractRoutes();
	Route decodeRoute(uint x, uint y);
	//uint wrappedPreviousPosition(uint pos, RelStateValue relState, uint spaceDimension);
	float* devCurrentCostFunction_;
	float* devPreviousCostFunction_;
	float* hostMaxCostFunctionValues_;//for now - could be changed(creating 0/1 map on device and let host just check for ones)
	float* devMaxCostFunctionValues_;
	unsigned short* devRoutes_;
	unsigned short* hostMaxCostFunctionRoutes_;
	unsigned short* devMaxCostFunctionRoutes_;
	RelStateIndexValue* hostMaxCostFunctionRelStatesIndexes_;
	RelStateIndexValue* devMaxCostFunctionRelStatesIndexes_;
	float detectionThreshold_;
	std::vector<Route> detectedRoutes_;
};