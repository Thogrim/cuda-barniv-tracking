#pragma once

#include <qobject.h>
#include <qevent.h>

class FocusSignalSender : public QObject{
	Q_OBJECT

public:
	explicit FocusSignalSender(QObject* parent = nullptr)
		:QObject(parent){
		if (parent)
			parent->installEventFilter(this);
	}
	bool eventFilter(QObject *obj, QEvent *event) override {
		Q_UNUSED(obj);
		if (event->type() == QEvent::FocusIn)
			emit focusChanged(true);
		else if (event->type() == QEvent::FocusOut)
			emit focusChanged(false);

		return false;
	}

signals:
	void focusChanged(bool in);
};