#include "CudaTrackingQtApp.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CudaTrackingQtApp w;
    w.show();
    return a.exec();
}
