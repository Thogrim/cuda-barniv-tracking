#include "RoutesScene.h"

#include <qgraphicsitem.h>
#include <QGraphicsSceneMouseEvent>

#include "../GraphicsItems/RouteGraphicsItem.h"

RoutesScene::RoutesScene(QObject * parent):
	QGraphicsScene(parent),
	mode_(Mode::MOVE_ROUTE),
	currentlyDrawnRoute_(nullptr),
	lines_(),
	lastMousePos_(),
	mouseMoved_(false),
	spaceBoundsRect_(nullptr){
	spaceBoundsRect_ = addRect(0, 0, 0, 0, QPen(Qt::magenta));
}

RoutesScene::~RoutesScene(){
}

void RoutesScene::setMode(Mode mode) {
	mode_ = mode;
}

void RoutesScene::setSpaceBounds(uint width, uint height){
	spaceBoundsRect_->setRect(0, 0, width, height);
}

void RoutesScene::mousePressEvent(QGraphicsSceneMouseEvent* event){
	if (event->button() != Qt::MouseButton::LeftButton)
		return;
	if (mode_ == Mode::INSERT_ROUTE) {
		currentlyDrawnRoute_ = new RouteGraphicsItem(0,0);
		currentlyDrawnRoute_->setFlag(QGraphicsItem::ItemIsMovable, true);
		currentlyDrawnRoute_->setFlag(QGraphicsItem::ItemIsSelectable, true);
		currentlyDrawnRoute_->setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
		currentlyDrawnRoute_->setColor(Qt::green);
		currentlyDrawnRoute_->setPos(event->scenePos());
		addItem(currentlyDrawnRoute_);
	}
	QGraphicsScene::mousePressEvent(event);
}

void RoutesScene::mouseMoveEvent(QGraphicsSceneMouseEvent* event){
	if (mode_ == Mode::INSERT_ROUTE && currentlyDrawnRoute_) {
		QPointF delta = event->scenePos()- currentlyDrawnRoute_->pos();
		for (int i = 0; i < NUMBER_OF_SPACES - 1; ++i) {
			currentlyDrawnRoute_->setPoint(i, (i+1)*delta/(NUMBER_OF_SPACES-1));
			currentlyDrawnRoute_->update();
		}
	}
	else if (mode_ == Mode::MOVE_ROUTE) {
		QGraphicsScene::mouseMoveEvent(event);
	}
}

void RoutesScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* event){
	if (mode_ == Mode::INSERT_ROUTE && currentlyDrawnRoute_) {
		emit routeInserted(currentlyDrawnRoute_);
		currentlyDrawnRoute_ = nullptr;
	}
	else if (mode_ == Mode::MOVE_ROUTE) {
		QGraphicsItem* item = itemAt(event->scenePos(),QTransform());
		if (item) {
			if (item->type() == RouteGraphicsItem::RouteType) {
				RouteGraphicsItem* route = qgraphicsitem_cast<RouteGraphicsItem*>(item);
				emit routeReleased(route);
			}
			else
				emit routeUnselected();
		}
		else
			emit routeUnselected();
	}
	QGraphicsScene::mouseReleaseEvent(event);
}
