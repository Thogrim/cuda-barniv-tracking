#pragma once

#include <qgraphicsscene.h>

class RouteGraphicsItem;

class RoutesScene : public QGraphicsScene{
	Q_OBJECT

public:
	enum class Mode {
		INSERT_ROUTE,
		MOVE_ROUTE
	};

	RoutesScene(QObject *parent = Q_NULLPTR);
	~RoutesScene();

public slots:
	void setMode(Mode mode);
	void setSpaceBounds(uint width, uint height);

signals:
	//void routeInserted(qreal x1, qreal y1, qreal x2, qreal y2);
	void routeInserted(const RouteGraphicsItem* routeItem);
	void routeReleased(const RouteGraphicsItem* routeItem);
	void routeUnselected();
	//void routeSelected(uint track);
	//void routeDeleted(uint track);

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
	void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) override;
	//void dragLeaveEvent

private:
	Mode mode_;
	RouteGraphicsItem* currentlyDrawnRoute_;
	//TrackGraphicsItem* currentlyMovedLine_;
	std::vector<RouteGraphicsItem*> lines_;
	QPointF lastMousePos_; //last position of mouse - used in mouse move event to count movement offset
	bool mouseMoved_; //flag indicating that mouse was moved between mouse press and release left button events - used in mouse release event
	QGraphicsRectItem* spaceBoundsRect_;
};