#pragma once

#include <qwidget.h>

class ColorMapWidget : public QWidget{
public:
	ColorMapWidget(QWidget *parent = Q_NULLPTR);
	~ColorMapWidget();

	void setColors(const std::vector<QColor>& colors);

protected:
	void paintEvent(QPaintEvent* e) override;

private:
	std::vector<QColor> colors_;
};