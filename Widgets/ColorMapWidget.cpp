#include "ColorMapWidget.h"

#include <qpainter.h>

ColorMapWidget::ColorMapWidget(QWidget * parent)
	:QWidget(parent){
}

ColorMapWidget::~ColorMapWidget(){
}

void ColorMapWidget::setColors(const std::vector<QColor>& colors){
	colors_ = colors;
	update();
}

void ColorMapWidget::paintEvent(QPaintEvent * e){
	uint colorCount = colors_.size();
	if (colorCount < 2)
		return;
	QRect rect(0, 0, width(), height());
	QLinearGradient gradient(rect.bottomLeft(), rect.topLeft());
	for (uint i = 0; i < colorCount; ++i) {
		gradient.setColorAt(static_cast<qreal>(i)/(colorCount-1), colors_[i]);
	}
	QPainter painter;
	painter.begin(this);
	painter.fillRect(rect, gradient);
	painter.end();
}
