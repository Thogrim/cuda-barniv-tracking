#include "CudaTrackingQtApp.h"

#include <QtWidgets/qmessagebox.h>
#include <cuda_runtime_api.h>
#include <random>
//#include <array>

#include "qcustomplot.h"
#include "Dialogs/ChooseDeviceDlg.h"
#include "Dialogs/EditRoutesDlg.h"
#include "Dialogs/ShowResultsDlg.h"
#include "CudaTrackingDrawer.h"

#define THROW_ON_CUDA_ERROR(expr) \
	error = expr; \
	if(error != cudaSuccess) \
		throw error;
 
CudaTrackingQtApp::CudaTrackingQtApp(QWidget *parent)
	: QMainWindow(parent),
	chosenDevice_(-1),
	devSpaces_(),
	hostSpaces_(),
	hostOneStateCostFunction_(nullptr),
	barniv_(),
	currentSpaceIndex_(-1),
	currentRelStateIndex_(-1),
	currentY_(-1),
	rdbGeneratedSignalClicked_(false),
	trackedObjects_(),
	resultObjects_(){
	ui_.setupUi(this);
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		devSpaces_[i] = nullptr;
		hostSpaces_[i] = nullptr;
	}

	ui_.cudaTrackingGlWidget_->setDrawable(this);
	ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(0.f, 30.f);
	//set gradient for gl widget
	unsigned int gradient[GRADIENT_COLORS_COUNT_];
	gradient[0] = 0xFFFF0000; //blue
	gradient[1] = 0xFFFFFF00; //cyan
	gradient[2] = 0xFF00FF00; //green
	gradient[3] = 0xFF00FFFF; //yellow
	gradient[4] = 0xFF0000FF; //red
	gradient[5] = 0x00000000; //last color must be 0
	ui_.cudaTrackingGlWidget_->setColorGradient(gradient);
	ui_.edtMinSignalValue_->setText(QString("0"));
	ui_.edtMaxSignalValue_->setText(QString("30"));
	//color map
	ui_.colorMap3_->setColors({ Qt::darkGreen,Qt::darkYellow ,Qt::darkRed});
	ui_.colorMap5_->setColors({ Qt::blue, Qt::cyan, Qt::green,Qt::yellow ,Qt::red });
	ui_.rdbColorMap3_->setChecked(false);
	ui_.rdbColorMap5_->setChecked(true);

	//disable controls
	ui_.btnGenerateSignal_->setDisabled(true);
	ui_.btnStartTracking_->setDisabled(true);
	ui_.btnNextSpace_->setDisabled(true);
	ui_.btnPreviousSpace_->setDisabled(true);
	ui_.btnNextRelativeState_->setDisabled(true);
	ui_.btnPreviousRelativeState_->setDisabled(true);
	ui_.rdbCostFunction_->setDisabled(true);
	ui_.rdbGeneratedSignal_->setDisabled(true);
	ui_.rdbMaxCostFunctionValues_->setDisabled(true);

	ui_.btnResetProgram_->setDisabled(true);

	QObject::connect(ui_.btnInitTracking_, &QPushButton::released, this, &CudaTrackingQtApp::onInitProgram);
	QObject::connect(ui_.btnResetProgram_, &QPushButton::released, this, &CudaTrackingQtApp::resetProgram);
	QObject::connect(ui_.btnGenerateSignal_, &QPushButton::released, this, &CudaTrackingQtApp::onGenerateSignal);
	QObject::connect(ui_.btnZoomIn_, &QPushButton::released, [this]() {
		int zoomValue = static_cast<int>(ui_.cudaTrackingGlWidget_->incrementZoom());
		ui_.lblCurrentZoomValue_->setText(QString::number(zoomValue));
	});
	QObject::connect(ui_.btnZoomOut_, &QPushButton::released, [this]() {
		int zoomValue = static_cast<int>(ui_.cudaTrackingGlWidget_->decrementZoom());
		ui_.lblCurrentZoomValue_->setText(QString::number(zoomValue));
	});
	QObject::connect(ui_.cudaTrackingGlWidget_, &CudaTrackingGlWidget::objectPressed, this, &CudaTrackingQtApp::onNewGlWidgetSelection);
	QObject::connect(ui_.btnChangeMaxMinSignalValues_, &QPushButton::released, this, &CudaTrackingQtApp::onChangeMinMaxSignalValues);
	QObject::connect(ui_.btnChooseDevice_, &QPushButton::released, this, &CudaTrackingQtApp::onChooseDevice);
	QObject::connect(ui_.btnPreviousSpace_, &QPushButton::released, [this]() {
		if (--currentSpaceIndex_ < 0)
			currentSpaceIndex_ = 0;
		ui_.lblCurrentSpaceValue_->setText(QString::number(currentSpaceIndex_));
		ui_.cudaTrackingGlWidget_->update();
	});
	QObject::connect(ui_.btnNextSpace_, &QPushButton::released, [this]() {
		if (++currentSpaceIndex_ > NUMBER_OF_SPACES - 1)
			currentSpaceIndex_ = NUMBER_OF_SPACES - 1;
		ui_.lblCurrentSpaceValue_->setText(QString::number(currentSpaceIndex_));
		ui_.cudaTrackingGlWidget_->update();
	});
	QObject::connect(ui_.btnNextRelativeState_, &QPushButton::released, [this]() {
		if (++currentRelStateIndex_ > barniv_.relStatesCount_ - 1)
			currentRelStateIndex_ = barniv_.relStatesCount_ - 1;
		size_t spaceSize = barniv_.spaceWidth_ * barniv_.spaceHeight_ * sizeof(float);
		cudaMemcpy(hostOneStateCostFunction_, barniv_.getDevCurrentCostFunction(currentRelStateIndex_), spaceSize, cudaMemcpyDeviceToHost);
		ui_.lblCurrentRelativeStateIndexValue_->setText(QString::number(currentRelStateIndex_));
		ui_.lblRelativeStateXValue_->setText(QString::number(barniv_.xRelStates_[currentRelStateIndex_]));
		ui_.lblRelativeStateYValue_->setText(QString::number(barniv_.yRelStates_[currentRelStateIndex_]));
		ui_.cudaTrackingGlWidget_->update();
	});
	QObject::connect(ui_.btnPreviousRelativeState_, &QPushButton::released, [this]() {
		if (--currentRelStateIndex_ < 0)
			currentRelStateIndex_ = 0;
		size_t spaceSize = barniv_.spaceWidth_ * barniv_.spaceHeight_ * sizeof(float);
		cudaMemcpy(hostOneStateCostFunction_, barniv_.getDevCurrentCostFunction(currentRelStateIndex_), spaceSize, cudaMemcpyDeviceToHost);
		ui_.lblCurrentRelativeStateIndexValue_->setText(QString::number(currentRelStateIndex_));
		ui_.lblRelativeStateXValue_->setText(QString::number(barniv_.xRelStates_[currentRelStateIndex_]));
		ui_.lblRelativeStateYValue_->setText(QString::number(barniv_.yRelStates_[currentRelStateIndex_]));
		ui_.cudaTrackingGlWidget_->update();
	});
	QObject::connect(ui_.btnStartTracking_, &QPushButton::released, this, &CudaTrackingQtApp::onStartTracking);
	//radio buttons slots
	QObject::connect(ui_.rdbGeneratedSignal_, &QRadioButton::clicked, [this](bool checked) {
		if (checked) {
			enableSpaceControls();
			disableCostFunctionControls();
			if (!rdbGeneratedSignalClicked_) {
				int maxSignal = ui_.edtMaxSignalValue_->text().toInt() / 5;
				int minSignal = ui_.edtMinSignalValue_->text().toInt() / 5;
				ui_.edtMaxSignalValue_->setText(QString::number(maxSignal));
				ui_.edtMinSignalValue_->setText(QString::number(minSignal));
				ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(minSignal, maxSignal);
			}
			rdbGeneratedSignalClicked_ = true;
			ui_.cudaTrackingGlWidget_->update();//important!
		}
	});
	QObject::connect(ui_.rdbCostFunction_, &QRadioButton::clicked, [this](bool checked) {
		if (checked) {
			enableCostFunctionControls();
			disableSpaceControls();
			if (rdbGeneratedSignalClicked_) {
				int maxSignal = ui_.edtMaxSignalValue_->text().toInt() * 5;
				int minSignal = ui_.edtMinSignalValue_->text().toInt() * 5;
				ui_.edtMaxSignalValue_->setText(QString::number(maxSignal));
				ui_.edtMinSignalValue_->setText(QString::number(minSignal));
				ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(minSignal, maxSignal);
			}
			rdbGeneratedSignalClicked_ = false;
			ui_.cudaTrackingGlWidget_->update();//important!
		}
	});
	QObject::connect(ui_.rdbMaxCostFunctionValues_, &QRadioButton::clicked, [this](bool checked) {
		disableSpaceControls();
		disableCostFunctionControls();
		if (rdbGeneratedSignalClicked_) {
			int maxSignal = ui_.edtMaxSignalValue_->text().toInt() * 5;
			int minSignal = ui_.edtMinSignalValue_->text().toInt() * 5;
			ui_.edtMaxSignalValue_->setText(QString::number(maxSignal));
			ui_.edtMinSignalValue_->setText(QString::number(minSignal));
			ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(minSignal, maxSignal);
		}
		rdbGeneratedSignalClicked_ = false;
		ui_.cudaTrackingGlWidget_->update();//important!
	});
	QObject::connect(ui_.btnEditTracks_, &QPushButton::released, this, &CudaTrackingQtApp::onEditTracks);
	QObject::connect(ui_.cudaTrackingPlotWidget_, &QCustomPlot::mouseRelease, this, &CudaTrackingQtApp::onPlotMousePress);
	QObject::connect(ui_.btnShowResults_, &QPushButton::released, this, [this]() {
		ShowResultsDlg dlg(barniv_.spaceWidth_, barniv_.spaceHeight_, trackedObjects_, resultObjects_, this);
		dlg.setModal(true);
		dlg.exec();
	});
	QObject::connect(ui_.rdbColorMap3_, &QRadioButton::clicked, [this](bool checked) {
		if (checked) {
			ui_.colorMap3_->setColors({ Qt::green,Qt::yellow ,Qt::red });
			ui_.colorMap5_->setColors({ Qt::darkBlue, Qt::darkCyan, Qt::darkGreen,Qt::darkYellow ,Qt::darkRed });
			ui_.rdbColorMap5_->setChecked(false);
			unsigned int gradient[GRADIENT_COLORS_COUNT_];
			gradient[0] = 0xFF00FF00; //green
			gradient[1] = 0xFF00FFFF; //yellow
			gradient[2] = 0xFF0000FF; //red
			gradient[3] = 0x00000000; //last color must be 0
			ui_.cudaTrackingGlWidget_->setColorGradient(gradient);
		}
	});
	QObject::connect(ui_.rdbColorMap5_, &QRadioButton::clicked, [this](bool checked) {
		if (checked) {
			ui_.colorMap3_->setColors({ Qt::darkGreen,Qt::darkYellow ,Qt::darkRed });
			ui_.colorMap5_->setColors({ Qt::blue, Qt::cyan, Qt::green,Qt::yellow ,Qt::red });
			ui_.rdbColorMap3_->setChecked(false);
			unsigned int gradient[GRADIENT_COLORS_COUNT_];
			gradient[0] = 0xFFFF0000; //blue
			gradient[1] = 0xFFFFFF00; //cyan
			gradient[2] = 0xFF00FF00; //green
			gradient[3] = 0xFF00FFFF; //yellow
			gradient[4] = 0xFF0000FF; //red
			gradient[5] = 0x00000000; //last color must be 0
			ui_.cudaTrackingGlWidget_->setColorGradient(gradient);
		}
	});
	initRelativeStates();

	// qcustomplot test:
	// generate some data:
	QVector<double> x(101), y(101); // initialize with entries 0..100
	for (int i = 0; i<101; ++i)
	{
		x[i] = i / 50.0 - 1; // x goes from -1 to 1
		y[i] = x[i] * x[i]; // let's plot a quadratic function
	}
	// create graph and assign data to it:
	ui_.cudaTrackingPlotWidget_->addGraph();
	ui_.cudaTrackingPlotWidget_->graph(0)->setData(x, y);
	ui_.cudaTrackingPlotWidget_->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
	// give the axes some labels:
	ui_.cudaTrackingPlotWidget_->xAxis->setLabel("x");
	ui_.cudaTrackingPlotWidget_->yAxis->setLabel("y");
	// set axes ranges, so we see all data:
	ui_.cudaTrackingPlotWidget_->xAxis->setRange(-1, 1);
	ui_.cudaTrackingPlotWidget_->yAxis->setRange(0, 1);
	ui_.cudaTrackingPlotWidget_->replot();
	//graph interactions
	ui_.cudaTrackingPlotWidget_->setInteraction(QCP::iRangeDrag, true);
	ui_.cudaTrackingPlotWidget_->setInteraction(QCP::iSelectPlottables, true);
}

CudaTrackingQtApp::~CudaTrackingQtApp() {
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		if (hostSpaces_[i])
			delete[] hostSpaces_[i];
		if (devSpaces_[i])
			cudaFree(devSpaces_[i]);
	}
	if(hostOneStateCostFunction_)
		delete[] hostOneStateCostFunction_;
}

void CudaTrackingQtApp::onBtnZoomIn(){
	int zoomValue = ui_.cudaTrackingGlWidget_->incrementZoom();
	ui_.lblCurrentZoomValue_->setText(QString::number(zoomValue));
}

void CudaTrackingQtApp::onBtnZoomOut(){
	int zoomValue = ui_.cudaTrackingGlWidget_->decrementZoom();
	ui_.lblCurrentZoomValue_->setText(QString::number(zoomValue));
}

void CudaTrackingQtApp::onNewGlWidgetSelection(int objectX, int objectY){
	auto spaceWidth = barniv_.spaceWidth_;
	ui_.cudaTrackingPlotWidget_->removeGraph(0);
	if (objectX >= 0 && objectX < spaceWidth && objectY >= 0 && objectY < spaceWidth) {
		currentY_ = objectY;
		QVector<double> objectValues(spaceWidth);
		QVector<double> xAxis(spaceWidth);
		const float* object;
		if (ui_.rdbGeneratedSignal_->isChecked())
			object = hostSpaces_[currentSpaceIndex_];
		else if (ui_.rdbCostFunction_->isChecked())
			object = hostOneStateCostFunction_; 
		else if (ui_.rdbMaxCostFunctionValues_->isChecked())
			object = barniv_.getHostMaxCostFunctionValues();
		object += objectY*spaceWidth;
		float max = -10000000;
		float temp;
		for (int i = 0; i < spaceWidth; ++i) {
			temp = object[i];
			objectValues[i] = temp;
			xAxis[i] = i;
			if (temp > max)
				max = temp;
		}
		QCPGraph* graph = ui_.cudaTrackingPlotWidget_->addGraph();
		graph->setData(xAxis, objectValues);
		graph->setScatterStyle(QCPScatterStyle::ssDisc);
		//center x axis on selection
		ui_.cudaTrackingPlotWidget_->xAxis->setRange(objectX-50, objectX+50);
		ui_.cudaTrackingPlotWidget_->yAxis->setRange(max-30, max+2);
	}
	ui_.cudaTrackingPlotWidget_->replot();
}

void CudaTrackingQtApp::onChangeMinMaxSignalValues(){	
	bool ok = false;
	int min = ui_.edtMinSignalValue_->text().toInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość minimalna sygnału!");
		return;
	}
	int max = ui_.edtMaxSignalValue_->text().toInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość maksymalna sygnału!");
		return;
	}
	if (min > max) {
		QMessageBox::critical(this, "CudaTracking Error", "Wartość maksymalna musi być większa od wartości minimalnej");
		return;
	}
	ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(min, max);
}

void CudaTrackingQtApp::onChooseDevice(){
	ChooseDeviceDlg dlg(this);
	dlg.setModal(true);
	dlg.exec();
	chosenDevice_ = dlg.getSelectedDevice();
	const QString& deviceName = dlg.getSelectedDeviceName();
	if (deviceName != "") {
		ui_.edtChosenDeviceName_->setText(deviceName);
		cudaSetDevice(chosenDevice_);
	}
}

void CudaTrackingQtApp::onInitProgram(){
	//check options
	bool ok = false;
	uint spaceWidth = ui_.edtSpaceWidth_->text().toUInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość szerokości przestrzeni!");
		return;
	}
	uint spaceHeight = ui_.edtSpaceHeight_->text().toUInt(&ok);
	if (!ok) {
		QMessageBox::critical(this, "CudaTracking Error", "Niepoprawna wartość wysokości przestrzeni!");
		return;
	}
	if (chosenDevice_ < 0) {
		QMessageBox::critical(this, "CudaTracking Error", "Nie wybrano urządzenia!");
		return;
	}
	//init cuda tracking
	try {
		cudaError error = cudaSuccess;
		barniv_.spaceWidth_ = spaceWidth;
		barniv_.spaceHeight_ = spaceHeight;
		if(!hostOneStateCostFunction_)
			hostOneStateCostFunction_ = new float[spaceWidth*spaceHeight];
		for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
			if (!devSpaces_[i])
				THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devSpaces_[i]), spaceWidth*spaceHeight * sizeof(float)));
		}
		THROW_ON_CUDA_ERROR(barniv_.initCudaTracking());
		THROW_ON_CUDA_ERROR(barniv_.setRelativeStatesOnDevice());
		ui_.btnGenerateSignal_->setDisabled(false);
		ui_.btnResetProgram_->setDisabled(false);
		ui_.btnInitTracking_->setDisabled(true);
	}
	catch (cudaError error) {
		resetProgram();
		//display message box
		QString errMsg("Wystąpił błąd CUDA podczas inicjacji algorytmu.\nKod błędu: ");
		errMsg.append(std::to_string(error).c_str());
		errMsg.append("\nKomunikat błędu: ");
		errMsg.append(cudaGetErrorString(error));
		QMessageBox::critical(this, "CudaTracking Error", errMsg);
		return;
	}
}

void CudaTrackingQtApp::onGenerateSignal(){
	float noiseLevel = ui_.edtNoiseLevel_->text().toFloat();
	noiseLevel = std::pow(10,(noiseLevel / 10.f));//convert noise level from dB to W
	std::random_device rd;
	std::mt19937 gen1(rd());
	std::weibull_distribution<float> distribution(2.f);//generates noise with rayleigh distribution
	auto width = barniv_.spaceWidth_;
	auto height = barniv_.spaceHeight_;
	for (unsigned int i = 0; i < NUMBER_OF_SPACES; ++i) {
		size_t spaceSize = width*height;
		if (!hostSpaces_[i])
			hostSpaces_[i] = new float[spaceSize];
		float* space = hostSpaces_[i];// = new float[spaceSize];
		for (unsigned int i = 0; i < spaceSize; ++i) {
			space[i] = 10 * log10(noiseLevel*distribution(gen1));
			//space[i] = 0;
		}
		for (const RouteInfo& obj : trackedObjects_) {
			//check if point is inside space bounds
			if (obj.x_[i] >= 0 && obj.x_[i] < width &&	obj.y_[i] >= 0 && obj.y_[i] < height)
				space[obj.x_[i] + obj.y_[i] * width] = obj.signal_;
		}
	}

	try {
		cudaError error = cudaSuccess;
		for (int i = 0; i < NUMBER_OF_SPACES; ++i)
			THROW_ON_CUDA_ERROR(cudaMemcpy(devSpaces_[i],hostSpaces_[i],width*height*sizeof(float),cudaMemcpyHostToDevice));
		//enable space controls
		ui_.rdbGeneratedSignal_->setChecked(true);
		ui_.rdbGeneratedSignal_->setDisabled(false);
		rdbGeneratedSignalClicked_ = true;
		ui_.btnNextSpace_->setDisabled(false);
		ui_.btnPreviousSpace_->setDisabled(false);
		currentSpaceIndex_ = 0;
		ui_.lblCurrentSpaceValue_->setText(QString::number(currentSpaceIndex_));
		ui_.btnStartTracking_->setDisabled(false);
		if (!rdbGeneratedSignalClicked_) {
			int maxSignal = ui_.edtMaxSignalValue_->text().toInt() / 5;
			int minSignal = ui_.edtMinSignalValue_->text().toInt() / 5;
			ui_.edtMaxSignalValue_->setText(QString::number(maxSignal));
			ui_.edtMinSignalValue_->setText(QString::number(minSignal));
			ui_.cudaTrackingGlWidget_->setNewMaxMinSignalValues(minSignal, maxSignal);
		}
		rdbGeneratedSignalClicked_ = true;
		ui_.cudaTrackingGlWidget_->update();//trigger repaint
	}
	catch (cudaError error) {
		QString errMsg("Wystąpił błąd CUDA podczas kopiowania danych wejściowych algorytmu.\nKod błędu: ");// + std::to_string(error) + "Komunikat błędu: " + cudaGetErrorString(error);
		errMsg.append(std::to_string(error).c_str());
		errMsg.append("Komunikat błędu: ");
		errMsg.append(cudaGetErrorString(error));
		QMessageBox::critical(this, "CudaTracking Error", errMsg);
		return;
	}
}

void CudaTrackingQtApp::onStartTracking(){
	try {
		cudaError error = cudaSuccess;
		THROW_ON_CUDA_ERROR(barniv_.performBarnivDpaTracking(devSpaces_));
		const std::vector<Route> detectedRoutes = barniv_.getDetectedRoutes();
		for (const Route& route : detectedRoutes) {
			resultObjects_.push_back(RouteInfo(route.x_[0], route.y_[0], route.x_[NUMBER_OF_SPACES-1], route.y_[NUMBER_OF_SPACES - 1], 0.f));
		}
		ui_.rdbCostFunction_->setDisabled(false);
		ui_.rdbMaxCostFunctionValues_->setDisabled(false);
		currentRelStateIndex_ = 0;
	}
	catch (cudaError error) {
		QString errMsg("Wystąpił błąd CUDA podczas wykonywania algorytmu.\nKod błędu: ");// + std::to_string(error) + "Komunikat błędu: " + cudaGetErrorString(error);
		errMsg.append(std::to_string(error).c_str());
		errMsg.append("Komunikat błędu: ");
		errMsg.append(cudaGetErrorString(error));
		QMessageBox::critical(this, "CudaTracking Error", errMsg);
		return;
	}
}

void CudaTrackingQtApp::onEditTracks(){
	EditRoutesDlg dlg(barniv_.spaceWidth_, barniv_.spaceHeight_, trackedObjects_, this);
	dlg.setModal(true);
	dlg.exec();
	trackedObjects_.clear();
	dlg.getRoutes(trackedObjects_);
	//get values from dlg and put them in main ui
	barniv_.spaceWidth_ = dlg.getSpaceWidth();
	barniv_.spaceHeight_ = dlg.getSpaceHeight();
	ui_.edtSpaceWidth_->setText(QString::number(barniv_.spaceWidth_));
	ui_.edtSpaceHeight_->setText(QString::number(barniv_.spaceHeight_));
	ui_.edtNoiseLevel_->setText(QString::number(dlg.getNoiseLevel()));
	//required memory
	size_t bytes = barniv_.getRequiredMemorySize();
	QString requiredMemory;
	if (bytes < 1024)
		requiredMemory = QString::number(bytes).append(" B");
	else if (bytes >= 1024 && bytes < 1024 * 1024)
		requiredMemory = QString::number(bytes >> 10, 'f', 2).append(" KiB");
	else if (bytes >= 1024 * 1024 && bytes < 1024 * 1024 * 1024)
		requiredMemory = QString::number(bytes >> 20, 'f', 2).append(" MiB");
	else if (bytes >= 1024 * 1024 * 1024)
		requiredMemory = QString::number(bytes >> 30, 'f', 2).append(" GiB");
	ui_.edtRequiredMemory_->setText(requiredMemory);
}

void CudaTrackingQtApp::onPlotMousePress(QMouseEvent * event){
	if (event->button() == Qt::RightButton){
		QCPAbstractPlottable* plottable = ui_.cudaTrackingPlotWidget_->plottableAt(event->pos());
		if (plottable){
			double x = ui_.cudaTrackingPlotWidget_->xAxis->pixelToCoord(event->pos().x());
			double y = ui_.cudaTrackingPlotWidget_->yAxis->pixelToCoord(event->pos().y());
			QCPGraph *graph = qobject_cast<QCPGraph*>(plottable);
			if (graph){
				double key = 0;
				double value = 0;
				bool ok = false;
				double m = std::numeric_limits<double>::max();
				auto data = graph->data();
				for(int i=0; i<graph->data()->size(); ++i){
					double d = qAbs(x - data->at(i)->key);
					if (d < m){
						key = data->at(i)->key;
						value = data->at(i)->value;
						ok = true;
						m = d;
					}
				}
				if (ok)	{
					QToolTip::hideText();
					QToolTip::showText(event->globalPos(),
						tr("<table>"
							"<tr>"
							"<td>[X,Y]:</td>" "<td>[%L1,%L2]</td>"
							"</tr>"
							"<tr>"
							"<td>sygnał(dB):</td>" "<td>%L3</td>"
							"</tr>"
							"</table>").
						arg(key).
						arg(currentY_).
						arg(value),
						ui_.cudaTrackingPlotWidget_, ui_.cudaTrackingPlotWidget_->rect());
				}
			}
		}
	}
}

void CudaTrackingQtApp::draw(const DrawingState & drawState) const{
	const float* devObject = nullptr;
	if (ui_.rdbGeneratedSignal_->isChecked())
		devObject = devSpaces_[currentSpaceIndex_];// hostSpaces_.getHostSpace(hostSpaces_.getCurrentSpaceIndex());
	else if (ui_.rdbCostFunction_->isChecked())
		devObject = barniv_.getDevCurrentCostFunction(currentRelStateIndex_); //costFunctionFragment_.getCostFunctionFragment();
	else if (ui_.rdbMaxCostFunctionValues_->isChecked())
		devObject = barniv_.getDevMaxCostFunctionValues(); //maxCostFunctionValues_.getMaxCostFunctionValues();
	if (devObject) {
		CudaTrackingDrawer drawer;
		drawer.drawObject(devObject, barniv_.spaceWidth_, barniv_.spaceHeight_, drawState);
	}
}

void CudaTrackingQtApp::initRelativeStates(){
	barniv_.relStatesCount_ = 4*7*7;
	barniv_.xRelStates_ = new RelStateValue[barniv_.relStatesCount_];
	barniv_.yRelStates_ = new RelStateValue[barniv_.relStatesCount_];
	int i = 0;
	for (int y = -10; y < 11; ++y) {
		for (int x = -10; x < 11; ++x) {
			if ((x < 4 && x >-4) || (y < 4 && y >-4))
				continue;
			barniv_.xRelStates_[i] = x;
			barniv_.yRelStates_[i] = y;
			++i;
		}
	}

	//for (int i = 0; i < 3; ++i) {
	//	xRelativeStates_[i] = i + 1;
	//	xRelativeStates_[i + 3] = i + 1;
	//	xRelativeStates_[i + 6] = i + 1;
	//	xRelativeStates_[i + 27] = i + 1;
	//	xRelativeStates_[i + 30] = i + 1;
	//	xRelativeStates_[i + 33] = i + 1;
	//	xRelativeStates_[i + 9] = i - 3;
	//	xRelativeStates_[i + 12] = i - 3;
	//	xRelativeStates_[i + 15] = i - 3;
	//	xRelativeStates_[i + 18] = i - 3;
	//	xRelativeStates_[i + 21] = i - 3;
	//	xRelativeStates_[i + 24] = i - 3;
	//}
	//for (int i = 0; i < 3; ++i) {
	//	yRelativeStates_[i * 3] = i + 1;
	//	yRelativeStates_[i * 3 + 1] = i + 1;
	//	yRelativeStates_[i * 3 + 2] = i + 1;
	//	yRelativeStates_[i * 3 + 9] = i + 1;
	//	yRelativeStates_[i * 3 + 10] = i + 1;
	//	yRelativeStates_[i * 3 + 11] = i + 1;
	//	yRelativeStates_[i * 3 + 18] = i - 3;
	//	yRelativeStates_[i * 3 + 19] = i - 3;
	//	yRelativeStates_[i * 3 + 20] = i - 3;
	//	yRelativeStates_[i * 3 + 27] = i - 3;
	//	yRelativeStates_[i * 3 + 28] = i - 3;
	//	yRelativeStates_[i * 3 + 29] = i - 3;
	//}
}

void CudaTrackingQtApp::resetProgram(){
	//free memory
	barniv_.uninitializeCudaTracking();
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		if (devSpaces_[i]) {
			cudaFree(devSpaces_[i]);
			devSpaces_[i] = nullptr;
		}
		if (hostSpaces_[i]) {
			delete[] hostSpaces_[i];
			hostSpaces_[i] = nullptr;
		}
	}
	if (hostOneStateCostFunction_) {
		delete[] hostOneStateCostFunction_;
		hostOneStateCostFunction_ = nullptr;
	}

	//reset controls state
	ui_.btnGenerateSignal_->setDisabled(true);
	ui_.btnStartTracking_->setDisabled(true);
	ui_.btnNextSpace_->setDisabled(true);
	ui_.btnPreviousSpace_->setDisabled(true);
	ui_.btnNextRelativeState_->setDisabled(true);
	ui_.btnPreviousRelativeState_->setDisabled(true);
	ui_.rdbCostFunction_->setDisabled(true);
	ui_.rdbGeneratedSignal_->setDisabled(true);
	ui_.rdbMaxCostFunctionValues_->setDisabled(true);
	ui_.rdbCostFunction_->setChecked(false);
	ui_.rdbGeneratedSignal_->setChecked(false);
	ui_.rdbMaxCostFunctionValues_->setChecked(false);
	disableCostFunctionControls();
	disableSpaceControls();
	ui_.edtSpaceWidth_->setText("");
	ui_.edtSpaceHeight_->setText("");
	ui_.edtNoiseLevel_->setText("");
	ui_.edtRequiredMemory_->setText("");

	currentSpaceIndex_ = 0;
	currentRelStateIndex_ = 0;
	currentY_ = 0;
	trackedObjects_.clear();
	resultObjects_.clear();

	ui_.btnResetProgram_->setDisabled(true);
	ui_.btnInitTracking_->setDisabled(false);
}

void CudaTrackingQtApp::enableSpaceControls(){
	ui_.btnNextSpace_->setDisabled(false);
	ui_.btnPreviousSpace_->setDisabled(false);
	ui_.lblCurrentSpaceValue_->setText(QString::number(currentSpaceIndex_));
}

void CudaTrackingQtApp::disableSpaceControls(){
	ui_.btnNextSpace_->setDisabled(true);
	ui_.btnPreviousSpace_->setDisabled(true);
	ui_.lblCurrentSpaceValue_->setText("-");
}

void CudaTrackingQtApp::enableCostFunctionControls(){
	ui_.btnPreviousRelativeState_->setDisabled(false);
	ui_.btnNextRelativeState_->setDisabled(false);
	ui_.lblCurrentRelativeStateIndexValue_->setText(QString::number(currentRelStateIndex_));
	ui_.lblRelativeStateXValue_->setText(QString::number(barniv_.xRelStates_[currentRelStateIndex_]));
	ui_.lblRelativeStateYValue_->setText(QString::number(barniv_.yRelStates_[currentRelStateIndex_]));
}

void CudaTrackingQtApp::disableCostFunctionControls(){
	ui_.btnPreviousRelativeState_->setDisabled(true);
	ui_.btnNextRelativeState_->setDisabled(true);
	ui_.lblCurrentRelativeStateIndexValue_->setText("-");
	ui_.lblRelativeStateXValue_->setText("-");
	ui_.lblRelativeStateYValue_->setText("-");
}