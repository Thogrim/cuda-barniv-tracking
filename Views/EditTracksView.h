#pragma once

#include <QtWidgets/QGraphicsView>
//for test
#include <qevent.h>

class EditTracksView : public QGraphicsView {
	Q_OBJECT

public:
	EditTracksView(QWidget *parent);
	~EditTracksView();

protected:
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;

private:
	int mousePressX_;
	int mousePressY_;
};
