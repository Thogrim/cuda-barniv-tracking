#include "EditTracksView.h"

#include <qevent.h>

EditTracksView::EditTracksView(QWidget *parent)
	: QGraphicsView(parent),
	mousePressX_(0),
	mousePressY_(0){
}

EditTracksView::~EditTracksView(){
}
void EditTracksView::mousePressEvent(QMouseEvent* event){
	if (event->button() == Qt::LeftButton) {
		mousePressX_ = event->x();
		mousePressY_ = event->y();
	}
	QGraphicsView::mousePressEvent(event);
}

void EditTracksView::mouseMoveEvent(QMouseEvent* event){
	if (event->buttons() & Qt::LeftButton) {
		QPointF oldp = mapToScene(mousePressX_, mousePressY_);
		QPointF newp = mapToScene(event->pos());
		QPointF translation = newp - oldp;

		//setTransformationAnchor(QGraphicsView::ViewportAnchor::NoAnchor);
		translate(translation.x(), translation.y());
		//setTra
		mousePressX_ = event->x();
		mousePressY_ = event->y();
	}
	QGraphicsView::mouseMoveEvent(event);
}