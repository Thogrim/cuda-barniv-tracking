#include "RouteGraphicsItem.h"

#include <qpainter.h>
#include <cassert>

#define PEN_WIDTH 1

RouteGraphicsItem::RouteGraphicsItem(QGraphicsItem * parent)
	:RouteGraphicsItem(0, 0, parent) {
}

RouteGraphicsItem::RouteGraphicsItem(const QPointF& lastPoint, QGraphicsItem *parent)
	: RouteGraphicsItem(lastPoint.x(), lastPoint.y(), parent) {
}

RouteGraphicsItem::RouteGraphicsItem(qreal lastX, qreal lastY, QGraphicsItem *parent)
	: QGraphicsItem(parent),
	points_(),
	color_(0, 0, 0, 255) {
	for (int i = 0; i < NUMBER_OF_GRAPHIC_LINES; ++i) 
		points_[i] = QPointF((i + 1)*lastX / (NUMBER_OF_GRAPHIC_LINES), (i + 1)*lastY / (NUMBER_OF_GRAPHIC_LINES));
}

RouteGraphicsItem::~RouteGraphicsItem() {
}

QRectF RouteGraphicsItem::boundingRect() const {
	const qreal adjust = PEN_WIDTH + 1;
	qreal xmin = 0;
	qreal xmax = 0;
	qreal ymin = 0;
	qreal ymax = 0;
	for (int i = 0; i < NUMBER_OF_GRAPHIC_LINES; ++i) {
		if (xmin > points_[i].x())
			xmin = points_[i].x();
		if (xmax < points_[i].x())
			xmax = points_[i].x();
		if (ymin > points_[i].y())
			ymin = points_[i].y();
		if (ymax < points_[i].y())
			ymax = points_[i].y();
	}
	return QRectF(QPointF(xmin - adjust, ymin - adjust), QPointF(xmax + adjust, ymax + adjust));
}

QPainterPath RouteGraphicsItem::shape() const {
	QPainterPath path;
	path.addRect(QRectF(QPointF(0, 0), points_[0]));
	for (int i = 0; i < NUMBER_OF_GRAPHIC_LINES-1; ++i)
		path.addRect(QRectF(points_[i], points_[i + 1]));
	return path;
}

int RouteGraphicsItem::type() const {
	return RouteType;
}

void RouteGraphicsItem::setColor(const QColor & color) {
	color_ = color;
}

void RouteGraphicsItem::setPoint(uint index, const QPointF & point) {
	assert(index < NUMBER_OF_GRAPHIC_LINES);
	points_[index] = point;
}

const QPointF & RouteGraphicsItem::getPoint(uint index) const {
	assert(index < NUMBER_OF_GRAPHIC_LINES);
	return points_[index];
}

void RouteGraphicsItem::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget) {
	//draw lines with half transparency
	int oldAlpha = color_.alpha();
	color_.setAlpha(oldAlpha / 2.f);
	QPen pen(color_);
	pen.setWidth(PEN_WIDTH);
	if (isSelected())
		pen.setStyle(Qt::PenStyle::DotLine);
	painter->setPen(pen);
	painter->drawLine(QPointF(0, 0), points_[0]);
	for (int i = 0; i < NUMBER_OF_GRAPHIC_LINES - 1; ++i)
		painter->drawLine(points_[i], points_[i + 1]);
	//draw points with full alpha
	color_.setAlpha(oldAlpha);
	pen.setColor(color_);
	pen.setStyle(Qt::PenStyle::SolidLine);
	painter->setPen(pen);
	painter->setBrush(QBrush(color_));
	painter->drawEllipse(QPointF(0, 0), PEN_WIDTH, PEN_WIDTH);
	for (int i = 0; i < NUMBER_OF_GRAPHIC_LINES; ++i)
		painter->drawEllipse(points_[i], PEN_WIDTH, PEN_WIDTH);
}