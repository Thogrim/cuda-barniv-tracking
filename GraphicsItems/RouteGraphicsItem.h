#pragma once

#include <qgraphicsitem.h>
#include <array>

#include "../CudaTrackingBarnivConstants.h"

#define NUMBER_OF_GRAPHIC_LINES (NUMBER_OF_SPACES-1)

class RouteGraphicsItem : public QGraphicsItem {
public:
	RouteGraphicsItem(QGraphicsItem *parent = nullptr);
	RouteGraphicsItem(const QPointF& lastPoint, QGraphicsItem *parent = nullptr);
	RouteGraphicsItem(qreal lastX, qreal lastY, QGraphicsItem *parent = nullptr);
	~RouteGraphicsItem();

	enum {
		RouteType = UserType + 1
	};

	QRectF boundingRect() const override;
	QPainterPath shape() const override;
	int type() const override;
	void setColor(const QColor& color);
	void setPoint(uint index, const QPointF& point);
	const QPointF& getPoint(uint index) const;

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

private:
	std::array<QPointF, NUMBER_OF_GRAPHIC_LINES> points_;
	QColor color_;
};