#pragma once

#include "CudaTrackingBarnivConstants.h"

struct RouteInfo {
	RouteInfo(int x1, int y1, int x2, int y2, float signal)
		:signal_(signal){
		for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
			x_[i] = x1 + i*(x2 - x1) / (NUMBER_OF_SPACES - 1);
			y_[i] = y1 + i*(y2 - y1) / (NUMBER_OF_SPACES - 1);
		}
	}
	RouteInfo(int x1, int y1, int x2, int y2)
		:RouteInfo(x1, y1, x2, y2, 0) {
	};
	int x_[NUMBER_OF_SPACES];
	int y_[NUMBER_OF_SPACES];
	float signal_; //signal in dB
};