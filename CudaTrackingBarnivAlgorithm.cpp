#include "CudaTrackingBarnivAlgorithm.h"

#include <cuda_runtime_api.h>

CudaTrackingBarnivAlgorithm::CudaTrackingBarnivAlgorithm():
	spaceWidth_(0),
	spaceHeight_(0),
	xRelStates_(nullptr),
	yRelStates_(nullptr),
	relStatesCount_(0),
	devCurrentCostFunction_(nullptr),
	devPreviousCostFunction_(nullptr),
	hostMaxCostFunctionValues_(nullptr),
	devMaxCostFunctionValues_(nullptr),
	devRoutes_(nullptr),
	hostMaxCostFunctionRoutes_(nullptr),
	devMaxCostFunctionRoutes_(nullptr),
	hostMaxCostFunctionRelStatesIndexes_(nullptr),
	devMaxCostFunctionRelStatesIndexes_(nullptr),
	detectionThreshold_(0),
	detectedRoutes_(){
}

CudaTrackingBarnivAlgorithm::~CudaTrackingBarnivAlgorithm(){
	uninitializeCudaTracking();
}

size_t CudaTrackingBarnivAlgorithm::getRequiredMemorySize() const{
	const size_t spaceSize = spaceHeight_*spaceWidth_;
	const size_t costFunctionSize = spaceSize*relStatesCount_;
	return costFunctionSize * (2*sizeof(float)+sizeof(unsigned short)) +
		spaceSize*(sizeof(float)+sizeof(unsigned short)+sizeof(RelStateIndexValue)+NUMBER_OF_SPACES*sizeof(float));
}

const float * CudaTrackingBarnivAlgorithm::getDevCurrentCostFunction(const RelStateIndexValue relStateIndex) const{
	return devCurrentCostFunction_ + spaceWidth_*spaceHeight_*relStateIndex;
}

const float * CudaTrackingBarnivAlgorithm::getDevPreviousCostFunction(const RelStateIndexValue relStateIndex) const{
	return devPreviousCostFunction_ + spaceWidth_*spaceHeight_*relStateIndex;
}

const float * CudaTrackingBarnivAlgorithm::getDevMaxCostFunctionValues() const{
	return devMaxCostFunctionValues_;
}

const float * CudaTrackingBarnivAlgorithm::getHostMaxCostFunctionValues() const{
	return hostMaxCostFunctionValues_;
}

const std::vector<Route>& CudaTrackingBarnivAlgorithm::getDetectedRoutes() const{
	return detectedRoutes_;
}

void CudaTrackingBarnivAlgorithm::uninitializeCudaTracking() {
	spaceWidth_ = 0;
	spaceHeight_ = 0;
	//relStatesCount_ = 0;
	freeDeviceMemory();
	//if (xRelStates_)
	//	delete[] xRelStates_;
	//if (yRelStates_)
	//	delete[] yRelStates_;
	if (hostMaxCostFunctionValues_)
		delete[] hostMaxCostFunctionValues_;
	if (hostMaxCostFunctionRoutes_)
		delete[] hostMaxCostFunctionRoutes_;
	if (hostMaxCostFunctionRelStatesIndexes_)
		delete[] hostMaxCostFunctionRelStatesIndexes_;

	devCurrentCostFunction_ = nullptr;
	devPreviousCostFunction_ = nullptr;
	hostMaxCostFunctionValues_ = nullptr;
	devMaxCostFunctionValues_ = nullptr;
	devRoutes_ = nullptr;
	hostMaxCostFunctionRoutes_ = nullptr;
	devMaxCostFunctionRoutes_ = nullptr;
	hostMaxCostFunctionRelStatesIndexes_ = nullptr;
	devMaxCostFunctionRelStatesIndexes_ = nullptr;

	detectedRoutes_.clear();
}

void CudaTrackingBarnivAlgorithm::freeDeviceMemory() {
	if (devCurrentCostFunction_)
		cudaFree(devCurrentCostFunction_);
	if (devPreviousCostFunction_)
		cudaFree(devPreviousCostFunction_);
	if (devMaxCostFunctionValues_)
		cudaFree(devMaxCostFunctionValues_);
	if (devRoutes_)
		cudaFree(devRoutes_);
	if (devMaxCostFunctionRoutes_)
		cudaFree(devMaxCostFunctionRoutes_);
	if (devMaxCostFunctionRelStatesIndexes_)
		cudaFree(devMaxCostFunctionRelStatesIndexes_);
}

void CudaTrackingBarnivAlgorithm::extractRoutes() {
	float* maxValues = hostMaxCostFunctionValues_;
	//TODO edge cases
	for (unsigned int y = 1; y < spaceHeight_ - 1; ++y) {
		float leftNeighbour = maxValues[y*spaceWidth_];
		float currentValue = maxValues[y*spaceWidth_ + 1];
		//float rightNeighbour = maxValues[y*spaceWidth_ + 2];
		for (unsigned int x = 1; x < spaceWidth_ - 1; ++x) {
			float rightNeighbour = maxValues[y*spaceWidth_ + x + 1];
			if (currentValue > leftNeighbour) {
				if (currentValue > rightNeighbour) {
					//sum neighbours values
					float neighboursSum = leftNeighbour + rightNeighbour;
					neighboursSum += maxValues[y*(spaceWidth_ + 1) - 1];
					neighboursSum += maxValues[y*(spaceWidth_ + 1)];
					neighboursSum += maxValues[y*(spaceWidth_ + 1) + 1];
					neighboursSum += maxValues[y*(spaceWidth_ - 1) - 1];
					neighboursSum += maxValues[y*(spaceWidth_ - 1)];
					neighboursSum += maxValues[y*(spaceWidth_ - 1) + 1];
					//check if currentValue is bigger than average value of neighbours by threshold
					if (currentValue - neighboursSum / 8 > detectionThreshold_) {
						detectedRoutes_.push_back(decodeRoute(x, y));
					}
					//no need to check next cell since its value will be lower than left neighbour
					++x;
					leftNeighbour = rightNeighbour;
					if (x<spaceWidth_ - 1)
						currentValue = maxValues[y*spaceWidth_ + x];
				}
				else {
					leftNeighbour = currentValue;
					currentValue = rightNeighbour;
				}
			}
			else {
				leftNeighbour = currentValue;
				currentValue = rightNeighbour;
			}
		}
	}
}

Route CudaTrackingBarnivAlgorithm::decodeRoute(uint x, uint y) {
	Route route;
	route.x_[NUMBER_OF_SPACES-1] = x;
	route.y_[NUMBER_OF_SPACES-1] = y;
	RelStateIndexValue relStateIndex = hostMaxCostFunctionRelStatesIndexes_[spaceWidth_*y + x];
	RelStateValue xRelState = xRelStates_[relStateIndex];
	RelStateValue yRelState = yRelStates_[relStateIndex];
	//wrap 2.0
	if (xRelState < 0) {
		RelStateValue relState = -xRelState;
		if (x + relState <= spaceWidth_)
			x += relState;
		else
			x = x + relState - spaceWidth_;
	}
	else {
		if (xRelState < x)
			x -= xRelState;
		else {
			x = spaceWidth_ + x - xRelState;
		}
	}
	if (yRelState < 0) {
		RelStateValue relState = -yRelState;
		if (y + relState <= spaceHeight_)
			y += relState;
		else
			y = y + relState - spaceHeight_;
	}
	else {
		if (yRelState < y)
			y -= yRelState;
		else {
			y = spaceHeight_ + y - yRelState;
		}
	}
	//wrap
	//if (xRelState > x)
	//	x = spaceWidth_ - (xRelState - x);
	//else if (x + xRelState > spaceWidth_)
	//	x = xRelState - (spaceWidth_ - x);
	//else
	//	x -= xRelState;
	//if (yRelState > y)
	//	y = spaceHeight_ - (yRelState - y);
	//else if (y + yRelState > spaceHeight_)
	//	y = yRelState - (spaceHeight_ - y);
	//else
	//	y -= yRelState;
	route.x_[NUMBER_OF_SPACES-2] = x;
	route.y_[NUMBER_OF_SPACES - 2] = y;
	unsigned short maxCostFunctionRoute = hostMaxCostFunctionRoutes_[spaceWidth_*y + x];
	for (int i = 0; i < NUMBER_OF_SPACES - 2; ++i) {
		unsigned short bitmask = 0x000F << (4*i);
		char relStateDiffId = (maxCostFunctionRoute & bitmask) >> (4*i);
		xRelState -= (relStateDiffId % 3) - 1;
		yRelState -= (relStateDiffId / 3) - 1;
		//wrap
		//if (xRelState > x)
		//	x = spaceWidth_ - (xRelState - x);
		//else if (x + xRelState > spaceWidth_)
		//	x = xRelState - (spaceWidth_ - x);
		//else
		//	x -= xRelState;
		//if (yRelState > y)
		//	y = spaceHeight_ - (yRelState - y);
		//else if (y + yRelState > spaceHeight_)
		//	y = yRelState - (spaceHeight_ - y);
		//else
		//	y -= yRelState;
		//wrap 2.0
		if (xRelState < 0) {
			RelStateValue relState = -xRelState;
			if (x + relState <= spaceWidth_)
				x += relState;
			else
				x = x + relState - spaceWidth_;
		}
		else {
			if (xRelState < x)
				x -= xRelState;
			else {
				x = spaceWidth_ + x - xRelState;
			}
		}
		if (yRelState < 0) {
			RelStateValue relState = -yRelState;
			if (y + relState <= spaceHeight_)
				y += relState;
			else
				y = y + relState - spaceHeight_;
		}
		else {
			if (yRelState < y)
				y -= yRelState;
			else {
				y = spaceHeight_ + y - yRelState;
			}
		}
		route.x_[NUMBER_OF_SPACES - 3 - i] = x;
		route.y_[NUMBER_OF_SPACES - 3 - i] = y;
	}
	return route;
}
//
//uint CudaTrackingBarnivAlgorithm::wrappedPreviousPosition(uint pos, RelStateValue relState, uint spaceDimension){
//
//}
