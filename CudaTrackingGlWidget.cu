//#include "CudaTrackingGlWidget.h"
//
//#include <cuda_runtime_api.h>
//#include <device_launch_parameters.h>
//
//__global__ void k_drawSelectionLine(uint* mappedPtr, uint displayWidth, uint displayHeight, int displayX, int displayY) {
//	uint tx = blockIdx.x*blockDim.x + threadIdx.x;
//	if (tx < displayWidth) {
//		uint oldColor = mappedPtr[displayY*displayWidth + displayX];
//		//invert color
//		uint newColor = 0xFF000000 | ~(0x00FF0000 & oldColor) | ~(0x0000FF00 & oldColor) | ~(0x000000FF & oldColor);
//		mappedPtr[displayY*displayWidth + tx] = newColor;
//	}
//}
//
//void CudaTrackingGlWidget::drawSelectionLine(uint* mappedPtr, int displayX, int displayY) {
//	uint blockWidth = 32;
//	uint gridWidth = width() / blockWidth + 1;
//	dim3 blockSize(blockWidth, 1);
//	dim3 gridSize(gridWidth, 1);
//	k_drawSelectionLine << <gridSize, blockSize >> > (mappedPtr, width(), height(), displayX, displayY);
//}