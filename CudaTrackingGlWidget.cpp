#include "CudaTrackingGlWidget.h"

#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <QtGui/qevent.h>

#include "CudaTrackingDrawable.h"

CudaTrackingGlWidget::CudaTrackingGlWidget(QWidget *parent)
	: QOpenGLWidget(parent),
	pbo_(0),
	cudaPboResource_(nullptr),
	drawState_(),
	drawable_(nullptr),
	mousePressX_(0),
	mousePressY_(0){
}

CudaTrackingGlWidget::~CudaTrackingGlWidget(){
	cudaGraphicsUnregisterResource(cudaPboResource_);
	glDeleteBuffers(1, &pbo_);
}

float CudaTrackingGlWidget::incrementZoom(){
	float oldZoom = drawState_.zoom_;
	if (++drawState_.zoom_ > 10.f)
		drawState_.zoom_ = 10.f;
	drawState_.xOffset_ += width() / (2 * oldZoom) - width() / (2 * drawState_.zoom_);
	drawState_.yOffset_ += height() / (2 * oldZoom) - height() / (2 * drawState_.zoom_);
	update();//always call this function instead of paintGL to trigger repaint manually
	return drawState_.zoom_;
}

float CudaTrackingGlWidget::decrementZoom(){
	float oldZoom = drawState_.zoom_;
	if (--drawState_.zoom_ < 1.f)
		drawState_.zoom_ = 1.f;
	drawState_.xOffset_ -= width() / (2 * drawState_.zoom_) - width() / (2 * oldZoom);
	drawState_.yOffset_ -= height() / (2 * drawState_.zoom_) - height() / (2 * oldZoom);
	update();//always call this function instead of paintGL to trigger repaint manually
	return drawState_.zoom_;
}

bool CudaTrackingGlWidget::setNewMaxMinSignalValues(float min, float max){
	if (min > max)
		return false;
	drawState_.minSignal_ = min;
	drawState_.maxSignal_ = max;
	update();
	return true;
}

void CudaTrackingGlWidget::setDrawable(CudaTrackingDrawable* drawable){
	drawable_ = drawable;
}

void CudaTrackingGlWidget::setColorGradient(unsigned int gradient[GRADIENT_COLORS_COUNT_]){
	for (int i = 0; i < GRADIENT_COLORS_COUNT_; ++i)
		drawState_.colorGradient_[i] = gradient[i];
	update();
}

void CudaTrackingGlWidget::initializeGL(){
	initializeOpenGLFunctions();
	glOrtho(0, width(), 0, height(), 0, 1);
	glViewport(0, 0, width(), height());
	//create pbo
	glGenBuffers(1, &pbo_);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo_);
	glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width()*height() * sizeof(GLubyte) * 4, 0, GL_STREAM_DRAW_ARB);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	//register pbo with cuda
	cudaError_t err = cudaGraphicsGLRegisterBuffer(&cudaPboResource_, pbo_, cudaGraphicsMapFlagsWriteDiscard);
	//save draw state
	drawState_.pboWidth_ = width();
	drawState_.pboHeight_ = height();
}

void CudaTrackingGlWidget::paintGL() {
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT);
	if (drawable_) {
		cudaGraphicsMapResources(1, &cudaPboResource_, 0);
		size_t num_bytes;
		cudaGraphicsResourceGetMappedPointer((void **)&drawState_.mappedPboPtr_, &num_bytes, cudaPboResource_);
		drawable_->draw(drawState_);
		cudaGraphicsUnmapResources(1, &cudaPboResource_, 0);
		glRasterPos2i(0, 0);
		glViewport(0, 0, width(), height());
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo_);
		glDrawPixels(width(), height(), GL_RGBA, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	}
}

void CudaTrackingGlWidget::resizeGL(int width, int height){
	glViewport(0, 0, width, height);
}

void CudaTrackingGlWidget::mouseMoveEvent(QMouseEvent * event){
	if (event->buttons() & Qt::LeftButton) {//left button pressed down
		int dx = mousePressX_ - event->x();
		int dy = mousePressY_ - event->y();
		drawState_.xOffset_ += dx;
		drawState_.yOffset_ -= dy;
		mousePressX_ = event->x();
		mousePressY_ = event->y();
		update();//triggers paintGL
	}
}

void CudaTrackingGlWidget::mousePressEvent(QMouseEvent * event){
	if (event->button() == Qt::LeftButton) {
		mousePressX_ = event->x();
		mousePressY_ = event->y();
	}
	else if (event->button() == Qt::RightButton) {
		if (drawable_) {
			//translate mouse coords to object coords
			int objectX = event->x() / drawState_.zoom_ + drawState_.xOffset_;
			int objectY = (height() - event->y()) / drawState_.zoom_ + drawState_.yOffset_;
			emit objectPressed(objectX, objectY);
		}
	}
}