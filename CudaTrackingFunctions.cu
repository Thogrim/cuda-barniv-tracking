#include <cuda_runtime.h>
#include <device_launch_parameters.h>

//float* devTestArray_;
//const unsigned int SIZE_ = 300;

bool initDevice() {
	return cudaSetDevice(0) == cudaSuccess;
}

bool allocateMemoryForDevice() {
	return true;
}

__global__ void testKernel(unsigned int* mappedPtr, unsigned int width, unsigned int height) {
	unsigned int tx = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned int ty = threadIdx.y + blockIdx.y*blockDim.y;
	if (tx<width && ty<height) {
		unsigned int value = 0;
		if (tx < width / 3)								
			value = 0xFF7F0000;//red
		else if (tx >= width / 3 && tx < 2 * width/ 3)	
			value = 0xFF007F00;//green
		else											
			value = 0xFF00007F;//blue
		mappedPtr[tx + ty*width] = value;
	}
}

bool performCudaTest(unsigned int* mappedPtr, unsigned int width, unsigned int height) {
	const int BLOCK_SIZE = 32;
	testKernel << <dim3(width / BLOCK_SIZE + 1, height / BLOCK_SIZE + 1), dim3(BLOCK_SIZE, BLOCK_SIZE) >> > (mappedPtr,width,height);
	return true;
}

bool cleanupDevice() {
	//cudaFree(devTestArray_);
	return true;
}