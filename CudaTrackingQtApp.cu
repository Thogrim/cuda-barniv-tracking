#include "CudaTrackingQtApp.h"

#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>


__constant__ RelStateValue DEV_REL_STATES_X_[DEV_MAX_NUMBER_OF_REL_STATES];
__constant__ RelStateValue DEV_REL_STATES_Y_[DEV_MAX_NUMBER_OF_REL_STATES];

__global__ void performBarnivDpaTrackingStep(float* nextCostFunction, float* previousCostFunction, float* space, uint spaceWidth, uint spaceHeight,
	uint statesCount, uint step, RelStateIndexValue initTopRightNeighbour, unsigned short* tracks) {
	uint currentX = blockIdx.x*blockDim.x + threadIdx.x;
	uint currentY = blockIdx.y*blockDim.y + threadIdx.y;
	//check bounds
	if (currentX > spaceWidth - 1 || currentY > spaceHeight - 1)
		return;
	//transition probabilities(logarithms)
	//const float transitionProbabilities[9] = {
	//	-20 * log10f(16), -20 * log10f(8), -20 * log10f(16),
	//	-20 * log10f(8), -20 * log10f(4), -20 * log10f(8),
	//	-20 * log10f(16), -20 * log10f(8), -20 * log10f(16)
	//};
	float signalValue = space[currentX + spaceWidth*currentY];
	//set up pointers
	RelStateIndexValue bottomNeighbour = 0;
	RelStateIndexValue topNeighbour = initTopRightNeighbour;
	uint spaceSize = spaceWidth*spaceHeight;
	for (RelStateIndexValue relStateIndex = 0; relStateIndex < statesCount; ++relStateIndex) {
		//get next relative state value and find set its neighbour indexes
		RelStateValue xRelState = DEV_REL_STATES_X_[relStateIndex];
		RelStateValue yRelState = DEV_REL_STATES_Y_[relStateIndex];
		//find top right and bottom right neighbour
		while (((DEV_REL_STATES_Y_[topNeighbour] == yRelState + 1 && DEV_REL_STATES_X_[topNeighbour] < xRelState + 1) ||
			DEV_REL_STATES_Y_[topNeighbour] < yRelState + 1) &&
			//topNeighbour < statesCount + 2)
			topNeighbour < statesCount)
			++topNeighbour;
		while (((DEV_REL_STATES_Y_[bottomNeighbour] == yRelState - 1 && DEV_REL_STATES_X_[bottomNeighbour] < xRelState + 1) ||
			DEV_REL_STATES_Y_[bottomNeighbour] < yRelState - 1) &&
			//bottomNeighbour < statesCount + 2)
			bottomNeighbour < statesCount)
			++bottomNeighbour;
		//read previous position
		int prevX = currentX - xRelState;
		int prevY = currentY - yRelState;
		//wrap coords
		if (prevX < 0)
			prevX += spaceWidth;
		else if (prevX > spaceWidth - 1)
			prevX -= spaceWidth;
		if (prevY < 0)
			prevY += spaceHeight;
		else if (prevY > spaceHeight - 1)
			prevY -= spaceHeight;
		//new code
		uint positionOffset = prevY*spaceWidth + prevX;
		float tempCostFunctionValue;
		float nextCostFunctionValue = previousCostFunction[relStateIndex*spaceSize + positionOffset];
		RelStateIndexValue previousRelStateIndex = relStateIndex;
		unsigned short relStateDiffId = 4;
		//check bottom right neighbour
		RelStateValue tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		RelStateValue tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState + 1 && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 2;
			}
		}
		//check bottom neighbour
		if (bottomNeighbour > 0)
			--bottomNeighbour;
		tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 1;
			}
		}
		//check bottom left neighbour
		if (bottomNeighbour > 0)
			--bottomNeighbour;
		tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState - 1 && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 0;
			}
		}
		//check left neighbour
		if (relStateIndex > 0) {
			tempXRelState = DEV_REL_STATES_X_[relStateIndex - 1];
			tempYRelState = DEV_REL_STATES_Y_[relStateIndex - 1];
			if (tempXRelState == xRelState - 1 && tempYRelState == yRelState) {
				tempCostFunctionValue = previousCostFunction[(relStateIndex - 1)*spaceSize + positionOffset];
				if (tempCostFunctionValue > nextCostFunctionValue) {
					nextCostFunctionValue = tempCostFunctionValue;
					previousRelStateIndex = relStateIndex - 1;
					relStateDiffId = 3;
				}
			}
		}
		//check right neighbour
		if (relStateIndex < statesCount) {
			tempXRelState = DEV_REL_STATES_X_[relStateIndex + 1];
			tempYRelState = DEV_REL_STATES_Y_[relStateIndex + 1];
			if (tempXRelState == xRelState + 1 && tempYRelState == yRelState) {
				tempCostFunctionValue = previousCostFunction[(relStateIndex + 1) *spaceSize + positionOffset];
				if (tempCostFunctionValue > nextCostFunctionValue) {
					nextCostFunctionValue = tempCostFunctionValue;
					previousRelStateIndex = relStateIndex + 1;
					relStateDiffId = 5;
				}
			}
		}
		//check top right neighbour
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState - 1 && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 8;
			}
		}
		//check top neighbour
		if (topNeighbour > 0)
			--topNeighbour;
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 7;
			}
		}
		//check top left neighbour
		if (topNeighbour > 0)
			--topNeighbour;
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState + 1 && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 6;
			}
		}
		//end of new code 
		//step 4 - save new cost function value that is
		//take the maximum value from step 3 and multiply it by signal value in this tile;
		//again we are dealing with logarithms so multiplication becomes addition
		nextCostFunctionValue += signalValue;
		nextCostFunction[spaceSize*relStateIndex + spaceWidth*currentY + currentX] = nextCostFunctionValue;
		unsigned short currentRoute = tracks[previousRelStateIndex*spaceSize + prevY*spaceWidth + prevX];
		//insert relative state difference in proper position
		unsigned short bitmask = 0xF000 >> (4 * (NUMBER_OF_SPACES - step));
		relStateDiffId = (relStateDiffId << 12) >> (4 * (NUMBER_OF_SPACES - step));
		currentRoute = (currentRoute & (~bitmask)) | relStateDiffId;
		tracks[relStateIndex*spaceSize + currentY*spaceWidth + currentX] = currentRoute;
	}
}

__global__ void performCostFunctionMaxValuesSearch(float* costFunction, uint spaceWidth, uint spaceHeight, RelStateIndexValue relStatesCount,
	unsigned short* routes,	float* costFunctionMaxValues, unsigned short* maxCostFunctionRoutes, RelStateIndexValue* maxCostFunctionRelStateIndexes) {
	uint tx = blockIdx.x*blockDim.x + threadIdx.x;
	uint ty = blockIdx.y*blockDim.y + threadIdx.y;
	if (tx > spaceWidth - 1 || ty > spaceHeight - 1)
		return;
	float maxCfValue = -1000000.f;
	float temp = 0.f;
	uint maxCfValueIndex = 0;
	uint index = tx + ty*spaceWidth;
	RelStateIndexValue maxCfRelStateId = 0;
	for (RelStateIndexValue i = 0; i < relStatesCount; ++i) {
		temp = costFunction[index];
		if (temp > maxCfValue) {
			maxCfValue = temp;
			maxCfValueIndex = index;
			maxCfRelStateId = i;
		}
		index += spaceWidth * spaceHeight;
	}
	costFunctionMaxValues[tx + ty*spaceWidth] = maxCfValue;
	uint trackHistoryIndex = maxCfValueIndex;
	uint maxCfTrackHistoryIndex = (tx + ty*spaceWidth);
	maxCostFunctionRoutes[maxCfTrackHistoryIndex] = routes[trackHistoryIndex];
	maxCostFunctionRelStateIndexes[tx + ty*spaceWidth] = maxCfRelStateId;
}

//cudaError CudaTrackingQtApp::setRelStatesOnDevice(RelStateValue* xRelStates, RelStateValue * yRelStates, RelStateIndexValue relStatesCount){
//	std::unique_ptr<RelStateValue[]> tempXRelStates{ new RelStateValue[relStatesCount] };
//	std::unique_ptr<RelStateValue[]> tempYRelStates{ new RelStateValue[relStatesCount] };
//	std::copy(xRelStates,xRelStates+relStatesCount)
//	return cudaSuccess;
//}

void CudaTrackingQtApp::initializeCudaTracking() {
	try {
		THROW_ON_CUDA_ERROR(cudaSetDevice(chosenDevice_));
		size_t spaceSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * sizeof(float);
		for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
			THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devSpaces_[i]), spaceSize));
		}
		initRelativeStates();
		THROW_ON_CUDA_ERROR(cudaMemcpyToSymbol(DEV_REL_STATES_X_, algorithmObjects_.xRelStates_, algorithmObjects_.relStatesCount_ * sizeof(RelStateValue), 0, cudaMemcpyHostToDevice));
		THROW_ON_CUDA_ERROR(cudaMemcpyToSymbol(DEV_REL_STATES_Y_, algorithmObjects_.yRelStates_, algorithmObjects_.relStatesCount_ * sizeof(RelStateValue), 0, cudaMemcpyHostToDevice));
		size_t costFunctionSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * algorithmObjects_.relStatesCount_*sizeof(float);
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devCurrentCostFunction_), costFunctionSize));
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devPreviousCostFunction_), costFunctionSize));
		THROW_ON_CUDA_ERROR(cudaMemset(algorithmObjects_.devCurrentCostFunction_, 0, costFunctionSize));
		THROW_ON_CUDA_ERROR(cudaMemset(algorithmObjects_.devPreviousCostFunction_, 0, costFunctionSize));
		size_t costFunctionMaxValuesSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * sizeof(float);
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devMaxCostFunctionValues_), costFunctionMaxValuesSize));
		size_t routesSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * algorithmObjects_.relStatesCount_ * sizeof(unsigned short);
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devRoutes_), routesSize));
		size_t maxCostFunctionRoutesSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * sizeof(unsigned short);
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devMaxCostFunctionRoutes_), maxCostFunctionRoutesSize));
		size_t maxCostFunctionRelStateIndexes = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_ * sizeof(RelStateIndexValue);
		THROW_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&algorithmObjects_.devMaxCostFunctionRelStatesIndexes_), maxCostFunctionRelStateIndexes));
		algorithmObjects_.detectionThreshold_ = 10.f;
		algorithmObjects_.hostOneStateCostFunction_ = new float[algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_];
		algorithmObjects_.hostMaxCostFunctionValues_ = new float[algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_];
		algorithmObjects_.hostMaxCostFunctionRoutes_ = new unsigned short[algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_];
		algorithmObjects_.hostMaxCostFunctionRelStatesIndexes_ = new RelStateIndexValue[algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_];
	}
	catch (std::runtime_error& error) {
		QMessageBox::critical(this, "CudaTracking Error", error.what());
		freeDeviceMemory();
	}
}

cudaError CudaTrackingQtApp::performBarnivDpaTracking() {
	uint blockDimSize = 32;
	uint gridDimX = algorithmObjects_.spaceWidth_ / blockDimSize + 1;
	uint gridDimY = algorithmObjects_.spaceHeight_ / blockDimSize + 1;
	dim3 gridSize(gridDimX, gridDimY);
	dim3 blockSize(blockDimSize, blockDimSize);

	performBarnivDpaTrackingStep << <gridSize, blockSize >> > (algorithmObjects_.devCurrentCostFunction_, algorithmObjects_.devPreviousCostFunction_,
		algorithmObjects_.devSpaces_[0], algorithmObjects_.spaceWidth_, algorithmObjects_.spaceHeight_, algorithmObjects_.relStatesCount_,
		0, initTopRightNeighbour, algorithmObjects_.devRoutes_);
	float* temp = nullptr;
	for (int i = 1; i < NUMBER_OF_SPACES; ++i) {
		temp = algorithmObjects_.devCurrentCostFunction_;
		algorithmObjects_.devCurrentCostFunction_ = algorithmObjects_.devPreviousCostFunction_;
		algorithmObjects_.devPreviousCostFunction_ = temp;
		performBarnivDpaTrackingStep << <gridSize, blockSize >> > (algorithmObjects_.devCurrentCostFunction_, algorithmObjects_.devPreviousCostFunction_,
			algorithmObjects_.devSpaces_[i], algorithmObjects_.spaceWidth_, algorithmObjects_.spaceHeight_, algorithmObjects_.relStatesCount_,
			i, initTopRightNeighbour, algorithmObjects_.devRoutes_);
	}
	performCostFunctionMaxValuesSearch << < gridSize, blockSize >> > (algorithmObjects_.devCurrentCostFunction_, algorithmObjects_.spaceWidth_, algorithmObjects_.spaceHeight_, algorithmObjects_.relStatesCount_,
		algorithmObjects_.devRoutes_, algorithmObjects_.devCostFunctionMaxValues_, algorithmObjects_.devMaxCostFunctionRoutes_, algorithmObjects_.devMaxCostFunctionRelStatesIndexes_);
	cudaError error = cudaGetLastError();
	if (error != cudaSuccess)
		return;
	//copy objects to host
	size_t spaceSize = algorithmObjects_.spaceWidth_*algorithmObjects_.spaceHeight_;
	error = cudaMemcpy(algorithmObjects_.hostMaxCostFunctionValues_, algorithmObjects_.devMaxCostFunctionValues_, spaceSize * sizeof(float), cudaMemcpyDeviceToHost);
	if (error != cudaSuccess)
		return;
	error = cudaMemcpy(algorithmObjects_.hostMaxCostFunctionRoutes_, algorithmObjects_.devMaxCostFunctionRoutes_, spaceSize * sizeof(unsigned short), cudaMemcpyDeviceToHost);
	if (error != cudaSuccess)
		return;
	error = cudaMemcpy(algorithmObjects_.hostMaxCostFunctionRelStatesIndexes_, algorithmObjects_.devMaxCostFunctionRelStatesIndexes_, spaceSize * sizeof(RelStateIndexValue), cudaMemcpyDeviceToHost);
	if (error != cudaSuccess)
		return;
	//copy states from device?
	extractRoutes();
}

void CudaTrackingQtApp::extractRoutes(){
	float* maxValues = algorithmObjects_.hostMaxCostFunctionValues_;
	//TODO edge cases
	for (uint y = 1; y < algorithmObjects_.spaceHeight_-1; ++y) {
		float leftNeighbour = maxValues[y*algorithmObjects_.spaceWidth_];
		float currentValue = maxValues[y*algorithmObjects_.spaceWidth_ + 1];
		//float rightNeighbour = maxValues[y*algorithmObjects_.spaceWidth_ + 2];
		for (uint x = 1; x < algorithmObjects_.spaceWidth_-1; ++x) {
			float rightNeighbour = maxValues[y*algorithmObjects_.spaceWidth_ + x + 1];
			if (currentValue > leftNeighbour) {
				if (currentValue > rightNeighbour) {
					//sum neighbours values
					float neighboursSum = leftNeighbour + rightNeighbour;
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ + 1) - 1];
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ + 1)];
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ + 1) + 1];
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ - 1) - 1];
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ - 1)];
					neighboursSum += maxValues[y*(algorithmObjects_.spaceWidth_ - 1) + 1];
					//check if currentValue is bigger than average value of neighbours by threshold
					if (currentValue - neighboursSum / 8 > algorithmObjects_.detectionThreshold_)
						decodeRoute(x,y);
					//no need to check next cell since its value will be lower than left neighbour
					++x;
					leftNeighbour = rightNeighbour;
					if(x<algorithmObjects_.spaceWidth_-1)
						currentValue = maxValues[y*algorithmObjects_.spaceWidth_ + x];
				}
				else {
					leftNeighbour = currentValue;
					currentValue = rightNeighbour;
				}
			}
			else {
				leftNeighbour = currentValue;
				currentValue = rightNeighbour;
			}
		}
	}
}

void CudaTrackingQtApp::decodeRoute(uint x, uint y){
	Route route;
	route.x_[0] = x;
	route.y_[0] = y;
	RelStateIndexValue relStateIndex = algorithmObjects_.hostMaxCostFunctionRelStatesIndexes_[algorithmObjects_.spaceWidth_*y + x];
	RelStateValue xRelState = algorithmObjects_.xRelStates_[relStateIndex];
	RelStateValue yRelState = algorithmObjects_.yRelStates_[relStateIndex];
	//wrap
	if (xRelState > x)
		x = algorithmObjects_.spaceWidth_ - (xRelState - x);
	else if (x + xRelState > algorithmObjects_.spaceWidth_)
		x = xRelState - (algorithmObjects_.spaceWidth_ - x);
	else
		x -= xRelState;
	if (yRelState > y)
		y = algorithmObjects_.spaceHeight_ - (yRelState - y);
	else if (y + yRelState > algorithmObjects_.spaceHeight_)
		y = yRelState - (algorithmObjects_.spaceHeight_ - y);
	else
		y -= yRelState;
	route.x_[1] = x;
	route.y_[1] = y;
	unsigned short maxCostFunctionRoute = algorithmObjects_.hostMaxCostFunctionRoutes_[algorithmObjects_.spaceWidth_*y + x];
	for (int i = 0; i < NUMBER_OF_SPACES - 2; ++i) {
		unsigned short bitmask = 0x000F << i;
		char relStateDiffId = (maxCostFunctionRoute & bitmask) >> i;
		xRelState -= (relStateDiffId % 3) - 1;
		yRelState -= (relStateDiffId / 3) - 1;
		//wrap
		if (xRelState > x)
			x = algorithmObjects_.spaceWidth_ - (xRelState - x);
		else if (x + xRelState > algorithmObjects_.spaceWidth_)
			x = xRelState - (algorithmObjects_.spaceWidth_ - x);
		else
			x -= xRelState;
		if (yRelState > y)
			y = algorithmObjects_.spaceHeight_ - (yRelState - y);
		else if (y + yRelState > algorithmObjects_.spaceHeight_)
			y = yRelState - (algorithmObjects_.spaceHeight_ - y);
		else
			y -= yRelState;
		route.x_[i + 2] = x;
		route.y_[i + 2] = y;
	}
}

void CudaTrackingQtApp::freeDeviceMemory(){
	for (int i = 0; i < NUMBER_OF_SPACES; ++i) {
		if (algorithmObjects_.devSpaces_[i]) {
			cudaFree(algorithmObjects_.devSpaces_[i]);
		}
	}
	if (algorithmObjects_.devCurrentCostFunction_)
		cudaFree(algorithmObjects_.devCurrentCostFunction_);
	if (algorithmObjects_.devPreviousCostFunction_)
		cudaFree(algorithmObjects_.devPreviousCostFunction_);
	if (algorithmObjects_.devMaxCostFunctionValues_)
		cudaFree(algorithmObjects_.devMaxCostFunctionValues_);
	if (algorithmObjects_.devRoutes_)
		cudaFree(algorithmObjects_.devRoutes_);
	if (algorithmObjects_.devMaxCostFunctionRoutes_)
		cudaFree(algorithmObjects_.devMaxCostFunctionRoutes_);
	if (algorithmObjects_.devMaxCostFunctionRelStatesIndexes_)
		cudaFree(algorithmObjects_.devMaxCostFunctionRelStatesIndexes_);
}

__device__ uint interpolateColor(uint firstColor, uint secondColor, float colorWeight) {
	//red component
	unsigned char firstRedValue = firstColor & 0x000000FF;
	unsigned char secondRedValue = secondColor & 0x000000FF;
	unsigned char red = static_cast<unsigned char>(firstRedValue + (secondRedValue - firstRedValue)*colorWeight);
	//green component
	unsigned char firstGreenValue = (firstColor & 0x0000FF00) >> 8;
	unsigned char secondGreenValue = (secondColor & 0x0000FF00) >> 8;
	unsigned char green = static_cast<unsigned char>(firstGreenValue + (secondGreenValue - firstGreenValue)*colorWeight);
	//blue component
	unsigned char firstBlueValue = (firstColor & 0x00FF0000) >> 16;
	unsigned char secondBlueValue = (secondColor & 0x00FF0000) >> 16;
	unsigned char blue = static_cast<unsigned char>(firstBlueValue + (secondBlueValue - firstBlueValue)*colorWeight);
	return 0xFF000000 | (blue << 16) | (green << 8) | red;
}

__global__ void drawObject(const float* object, const uint objectWidth, const uint objectHeight, const DrawingState drawState, unsigned char colorCount) {
	uint tx = blockIdx.x*blockDim.x + threadIdx.x;
	uint ty = blockIdx.y*blockDim.y + threadIdx.y;
	if (tx >= drawState.pboWidth_ || ty >= drawState.pboHeight_)
		return;
	int objectX = static_cast<int>(tx / drawState.zoom_ + drawState.xOffset_);
	int objectY = static_cast<int>(ty / drawState.zoom_ + drawState.yOffset_);
	if (objectX < objectWidth && objectX >= 0 && objectY < objectHeight && objectY >= 0) {
		float value = object[objectX + objectWidth*objectY];
		uint color = 0x00000000;
		if (value >= drawState.maxSignal_)
			color = drawState.colorGradient_[colorCount - 1];
		else if (value <= drawState.minSignal_)
			color = drawState.colorGradient_[0];
		else {
			float normalizedValue = (value - drawState.minSignal_) / (drawState.maxSignal_ - drawState.minSignal_);
			unsigned char colorIndex = static_cast<unsigned char>((colorCount - 1)*normalizedValue);
			float colorWeight = normalizedValue*(colorCount - 1) - colorIndex;
			color = interpolateColor( drawState.colorGradient_[colorIndex], drawState.colorGradient_[colorIndex + 1], colorWeight);
		}
		drawState.mappedPboPtr_[ty*drawState.pboWidth_ + tx] = color;
	}
	else
		drawState.mappedPboPtr_[ty*drawState.pboWidth_ + tx] = 0x00000000;
}

void CudaTrackingQtApp::draw(const DrawingState& drawState) const {
	float* object;
	if (ui_.rdbGeneratedSignal_->isChecked()) {
		object = algorithmObjects_.devSpaces_[currentSpaceIndex_];
	}
	else if (ui_.rdbCostFunction_->isChecked()) {
		object = algorithmObjects_.devCurrentCostFunction_;
		object += algorithmObjects_.spaceWidth_ * algorithmObjects_.spaceHeight_*currentRelStateIndex_; //move cost function pointer to proper relative state
	}
	else if (ui_.rdbMaxCostFunctionValues_->isChecked()) {
		object = algorithmObjects_.devMaxCostFunctionValues_;
	}
	unsigned char colorCount = 0;
	while (drawState.colorGradient_[colorCount] != 0x00000000 && colorCount < GRADIENT_COLORS_COUNT_)
		++colorCount;
	if (colorCount < 2)
		return;
	uint blockDimSize = 32;
	uint gridDimX = drawState.pboWidth_ / blockDimSize + 1;
	uint gridDimY = drawState.pboHeight_ / blockDimSize + 1;
	dim3 gridSize(gridDimX, gridDimY);
	dim3 blockSize(blockDimSize, blockDimSize);
	drawObject << <gridSize, blockSize >> >(object, algorithmObjects_.spaceWidth_, algorithmObjects_.spaceHeight_, drawState, colorCount);
}

//bool CudaTrackingQtApp::checkRoute(uint x, uint y, float costFunctionValue) {
//	float* maxValues = algorithmObjects_.hostMaxCostFunctionValues_;
//	//count average value of neighbours
//
//}
