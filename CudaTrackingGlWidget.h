#pragma once

#include <qopenglwidget.h>
#include <qopenglfunctions_3_3_core.h>
#include "CudaTrackingDrawer.h"

struct cudaGraphicsResource;
class CudaTrackingDrawable;

class CudaTrackingGlWidget : public QOpenGLWidget, public QOpenGLFunctions_3_3_Core{
	Q_OBJECT
public:
	explicit CudaTrackingGlWidget(QWidget* parent = nullptr);
	~CudaTrackingGlWidget();

	float incrementZoom();
	float decrementZoom();
	bool setNewMaxMinSignalValues(float min, float max);
	void setDrawable(CudaTrackingDrawable* drawable);
	void setColorGradient(unsigned int gradient[GRADIENT_COLORS_COUNT_]);

signals:
	void objectPressed(int x, int y);

protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int width, int height) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;

private:
	GLuint pbo_;
	cudaGraphicsResource* cudaPboResource_;
	DrawingState drawState_;
	CudaTrackingDrawable* drawable_;
	int mousePressX_;	//x coord of last QMouseEvent when left button was pressed
	int mousePressY_;	//y coord of last QMouseEvent when left button was pressed
};