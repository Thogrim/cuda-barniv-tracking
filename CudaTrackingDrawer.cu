#include "CudaTrackingDrawer.h"

#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

typedef unsigned int uint;

__device__ uint interpolateColor(uint firstColor, uint secondColor, float colorWeight) {
	//red component
	unsigned char firstRedValue = firstColor & 0x000000FF;
	unsigned char secondRedValue = secondColor & 0x000000FF;
	unsigned char red = static_cast<unsigned char>(firstRedValue + (secondRedValue - firstRedValue)*colorWeight);
	//green component
	unsigned char firstGreenValue = (firstColor & 0x0000FF00) >> 8;
	unsigned char secondGreenValue = (secondColor & 0x0000FF00) >> 8;
	unsigned char green = static_cast<unsigned char>(firstGreenValue + (secondGreenValue - firstGreenValue)*colorWeight);
	//blue component
	unsigned char firstBlueValue = (firstColor & 0x00FF0000) >> 16;
	unsigned char secondBlueValue = (secondColor & 0x00FF0000) >> 16;
	unsigned char blue = static_cast<unsigned char>(firstBlueValue + (secondBlueValue - firstBlueValue)*colorWeight);
	return 0xFF000000 | (blue << 16) | (green << 8) | red;
}

__global__ void drawToPbo(const float* object, const uint objectWidth, const uint objectHeight, const DrawingState drawState, unsigned char colorCount) {
	uint tx = blockIdx.x*blockDim.x + threadIdx.x;
	uint ty = blockIdx.y*blockDim.y + threadIdx.y;
	if (tx >= drawState.pboWidth_ || ty >= drawState.pboHeight_)
		return;
	int objectX = static_cast<int>(tx / drawState.zoom_ + drawState.xOffset_);
	int objectY = static_cast<int>(ty / drawState.zoom_ + drawState.yOffset_);
	if (objectX < objectWidth && objectX >= 0 && objectY < objectHeight && objectY >= 0) {
		float value = object[objectX + objectWidth*objectY];
		uint color = 0x00000000;
		if (value >= drawState.maxSignal_)
			color = drawState.colorGradient_[colorCount - 1];
		else if (value <= drawState.minSignal_)
			color = drawState.colorGradient_[0];
		else {
			float normalizedValue = (value - drawState.minSignal_) / (drawState.maxSignal_ - drawState.minSignal_);
			unsigned char colorIndex = static_cast<unsigned char>((colorCount - 1)*normalizedValue);
			float colorWeight = normalizedValue*(colorCount - 1) - colorIndex;
			color = interpolateColor(drawState.colorGradient_[colorIndex], drawState.colorGradient_[colorIndex + 1], colorWeight);
		}
		drawState.mappedPboPtr_[ty*drawState.pboWidth_ + tx] = color;
	}
	else
		drawState.mappedPboPtr_[ty*drawState.pboWidth_ + tx] = 0x00000000;
}

void CudaTrackingDrawer::drawObject(const float* devObject, const uint objectWidth, const uint objectHeight, const DrawingState& state){
	unsigned char colorCount = 0;
	while (state.colorGradient_[colorCount] != 0x00000000 && colorCount < GRADIENT_COLORS_COUNT_)
		++colorCount;
	if (colorCount < 2)
		return;
	uint blockDimSize = 32;
	uint gridDimX = state.pboWidth_ / blockDimSize + 1;
	uint gridDimY = state.pboHeight_ / blockDimSize + 1;
	dim3 gridSize(gridDimX, gridDimY);
	dim3 blockSize(blockDimSize, blockDimSize);
	drawToPbo << <gridSize, blockSize >> >(devObject, objectWidth, objectHeight, state, colorCount);
}