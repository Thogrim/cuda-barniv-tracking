#include "CudaTrackingBarnivAlgorithm.h"

#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <cassert>

#define RETURN_ON_CUDA_ERROR(expr) \
	error = expr; \
	if(error != cudaSuccess) \
		return error;

__constant__ RelStateValue DEV_REL_STATES_X_[DEV_MAX_NUMBER_OF_REL_STATES];
__constant__ RelStateValue DEV_REL_STATES_Y_[DEV_MAX_NUMBER_OF_REL_STATES];

__global__ void performBarnivDpaTrackingStep(float* nextCostFunction, float* previousCostFunction, float* space, uint spaceWidth, uint spaceHeight,
	uint statesCount, uint step, RelStateIndexValue initTopRightNeighbour, unsigned short* tracks) {
	uint currentX = blockIdx.x*blockDim.x + threadIdx.x;
	uint currentY = blockIdx.y*blockDim.y + threadIdx.y;
	//check bounds
	if (currentX > spaceWidth - 1 || currentY > spaceHeight - 1)
		return;
	//transition probabilities(logarithms)
	//const float transitionProbabilities[9] = {
	//	-20 * log10f(16), -20 * log10f(8), -20 * log10f(16),
	//	-20 * log10f(8), -20 * log10f(4), -20 * log10f(8),
	//	-20 * log10f(16), -20 * log10f(8), -20 * log10f(16)
	//};
	float signalValue = space[currentX + spaceWidth*currentY];
	//set up indexes
	RelStateIndexValue bottomNeighbour = 0;
	RelStateIndexValue topNeighbour = initTopRightNeighbour;
	uint spaceSize = spaceWidth*spaceHeight;
	for (RelStateIndexValue relStateIndex = 0; relStateIndex < statesCount; ++relStateIndex) {
		//get next relative state value and find set its neighbour indexes
		RelStateValue xRelState = DEV_REL_STATES_X_[relStateIndex];
		RelStateValue yRelState = DEV_REL_STATES_Y_[relStateIndex];
		//find top right and bottom right neighbour
		while (((DEV_REL_STATES_Y_[topNeighbour] == yRelState + 1 && DEV_REL_STATES_X_[topNeighbour] < xRelState + 1) ||
			DEV_REL_STATES_Y_[topNeighbour] < yRelState + 1) &&
			topNeighbour < statesCount)
			++topNeighbour;
		while (((DEV_REL_STATES_Y_[bottomNeighbour] == yRelState - 1 && DEV_REL_STATES_X_[bottomNeighbour] < xRelState + 1) ||
			DEV_REL_STATES_Y_[bottomNeighbour] < yRelState - 1) &&
			bottomNeighbour < statesCount)
			++bottomNeighbour;
		//read previous position
		int prevX = currentX - xRelState;
		int prevY = currentY - yRelState;
		//wrap coords
		if (prevX < 0)
			prevX += spaceWidth;
		else if (prevX > spaceWidth - 1)
			prevX -= spaceWidth;
		if (prevY < 0)
			prevY += spaceHeight;
		else if (prevY > spaceHeight - 1)
			prevY -= spaceHeight;
		//new code
		uint positionOffset = prevY*spaceWidth + prevX;
		float tempCostFunctionValue;
		float nextCostFunctionValue = previousCostFunction[relStateIndex*spaceSize + positionOffset];
		RelStateIndexValue previousRelStateIndex = relStateIndex;
		unsigned short relStateDiffId = 4;
		//check bottom right neighbour
		RelStateValue tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		RelStateValue tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState + 1 && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 2;
			}
		}
		//check bottom neighbour
		if (bottomNeighbour > 0)
			--bottomNeighbour;
		tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 1;
			}
		}
		//check bottom left neighbour
		if (bottomNeighbour > 0)
			--bottomNeighbour;
		tempXRelState = DEV_REL_STATES_X_[bottomNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[bottomNeighbour];
		if (tempXRelState == xRelState - 1 && tempYRelState == yRelState - 1) {
			tempCostFunctionValue = previousCostFunction[bottomNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = bottomNeighbour;
				relStateDiffId = 0;
			}
		}
		//check left neighbour
		if (relStateIndex > 0) {
			tempXRelState = DEV_REL_STATES_X_[relStateIndex - 1];
			tempYRelState = DEV_REL_STATES_Y_[relStateIndex - 1];
			if (tempXRelState == xRelState - 1 && tempYRelState == yRelState) {
				tempCostFunctionValue = previousCostFunction[(relStateIndex - 1)*spaceSize + positionOffset];
				if (tempCostFunctionValue > nextCostFunctionValue) {
					nextCostFunctionValue = tempCostFunctionValue;
					previousRelStateIndex = relStateIndex - 1;
					relStateDiffId = 3;
				}
			}
		}
		//check right neighbour
		if (relStateIndex < statesCount) {
			tempXRelState = DEV_REL_STATES_X_[relStateIndex + 1];
			tempYRelState = DEV_REL_STATES_Y_[relStateIndex + 1];
			if (tempXRelState == xRelState + 1 && tempYRelState == yRelState) {
				tempCostFunctionValue = previousCostFunction[(relStateIndex + 1) *spaceSize + positionOffset];
				if (tempCostFunctionValue > nextCostFunctionValue) {
					nextCostFunctionValue = tempCostFunctionValue;
					previousRelStateIndex = relStateIndex + 1;
					relStateDiffId = 5;
				}
			}
		}
		//check top right neighbour
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState + 1 && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 8;
			}
		}
		//check top neighbour
		if (topNeighbour > 0)
			--topNeighbour;
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 7;
			}
		}
		//check top left neighbour
		if (topNeighbour > 0)
			--topNeighbour;
		tempXRelState = DEV_REL_STATES_X_[topNeighbour];
		tempYRelState = DEV_REL_STATES_Y_[topNeighbour];
		if (tempXRelState == xRelState - 1 && tempYRelState == yRelState + 1) {
			tempCostFunctionValue = previousCostFunction[topNeighbour*spaceSize + positionOffset];
			if (tempCostFunctionValue > nextCostFunctionValue) {
				nextCostFunctionValue = tempCostFunctionValue;
				previousRelStateIndex = topNeighbour;
				relStateDiffId = 6;
			}
		}
		//step 4 - save new cost function value that is
		//take the maximum value from step 3 and multiply it by signal value in this tile;
		//again we are dealing with logarithms so multiplication becomes addition
		nextCostFunctionValue += signalValue;
		nextCostFunction[spaceSize*relStateIndex + spaceWidth*currentY + currentX] = nextCostFunctionValue;
		unsigned short currentRoute = tracks[previousRelStateIndex*spaceSize + prevY*spaceWidth + prevX];
		//insert relative state difference in proper position
		unsigned short bitmask = 0xF000 >> (4 * (NUMBER_OF_SPACES - step));
		relStateDiffId = (relStateDiffId << 12) >> (4 * (NUMBER_OF_SPACES - step));
		currentRoute = (currentRoute & (~bitmask)) | relStateDiffId;
		tracks[relStateIndex*spaceSize + currentY*spaceWidth + currentX] = currentRoute;
	}
}

__global__ void performCostFunctionMaxValuesSearch(float* costFunction, uint spaceWidth, uint spaceHeight, RelStateIndexValue relStatesCount,
	unsigned short* routes, float* costFunctionMaxValues, unsigned short* maxCostFunctionRoutes, RelStateIndexValue* maxCostFunctionRelStateIndexes) {
	uint tx = blockIdx.x*blockDim.x + threadIdx.x;
	uint ty = blockIdx.y*blockDim.y + threadIdx.y;
	if (tx > spaceWidth - 1 || ty > spaceHeight - 1)
		return;
	float maxCfValue = -1000000.f;
	float temp = 0.f;
	uint maxCfValueIndex = 0;
	uint index = tx + ty*spaceWidth;
	RelStateIndexValue maxCfRelStateId = 0;
	for (RelStateIndexValue i = 0; i < relStatesCount; ++i) {
		temp = costFunction[index];
		if (temp > maxCfValue) {
			maxCfValue = temp;
			maxCfValueIndex = index;
			maxCfRelStateId = i;
		}
		index += spaceWidth * spaceHeight;
	}
	costFunctionMaxValues[tx + ty*spaceWidth] = maxCfValue;
	uint trackHistoryIndex = maxCfValueIndex;
	uint maxCfTrackHistoryIndex = (tx + ty*spaceWidth);
	maxCostFunctionRoutes[maxCfTrackHistoryIndex] = routes[trackHistoryIndex];
	maxCostFunctionRelStateIndexes[tx + ty*spaceWidth] = maxCfRelStateId;
}

cudaError CudaTrackingBarnivAlgorithm::setRelativeStatesOnDevice() const{
	assert(relStatesCount_<DEV_MAX_NUMBER_OF_REL_STATES && "Number of relative states exceeds maximum nuber of relative states on device");
	cudaError error = cudaSuccess;
	RETURN_ON_CUDA_ERROR(cudaMemcpyToSymbol(DEV_REL_STATES_X_, xRelStates_, relStatesCount_ * sizeof(RelStateValue), 0, cudaMemcpyHostToDevice));
	RETURN_ON_CUDA_ERROR(cudaMemcpyToSymbol(DEV_REL_STATES_Y_, yRelStates_, relStatesCount_ * sizeof(RelStateValue), 0, cudaMemcpyHostToDevice));
	return error;
}

cudaError CudaTrackingBarnivAlgorithm::initCudaTracking() {
	cudaError error = cudaSuccess;
	size_t costFunctionSize = spaceWidth_*spaceHeight_ * relStatesCount_ * sizeof(float);
	if(!devCurrentCostFunction_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devCurrentCostFunction_), costFunctionSize));
	if (!devPreviousCostFunction_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devPreviousCostFunction_), costFunctionSize));
	RETURN_ON_CUDA_ERROR(cudaMemset(devCurrentCostFunction_, 0, costFunctionSize));
	RETURN_ON_CUDA_ERROR(cudaMemset(devPreviousCostFunction_, 0, costFunctionSize));
	size_t costFunctionMaxValuesSize = spaceWidth_*spaceHeight_ * sizeof(float);
	if (!devMaxCostFunctionValues_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devMaxCostFunctionValues_), costFunctionMaxValuesSize));
	size_t routesSize = spaceWidth_*spaceHeight_ * relStatesCount_ * sizeof(unsigned short);
	if (!devRoutes_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devRoutes_), routesSize));
	size_t maxCostFunctionRoutesSize = spaceWidth_*spaceHeight_ * sizeof(unsigned short);
	if (!devMaxCostFunctionRoutes_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devMaxCostFunctionRoutes_), maxCostFunctionRoutesSize));
	size_t maxCostFunctionRelStateIndexes = spaceWidth_*spaceHeight_ * sizeof(RelStateIndexValue);
	if (!devMaxCostFunctionRelStatesIndexes_)
		RETURN_ON_CUDA_ERROR(cudaMalloc(reinterpret_cast<void**>(&devMaxCostFunctionRelStatesIndexes_), maxCostFunctionRelStateIndexes));
	detectionThreshold_ = 10.f;
	if(!hostMaxCostFunctionValues_)
		hostMaxCostFunctionValues_ = new float[spaceWidth_*spaceHeight_];
	if (!hostMaxCostFunctionRoutes_)
		hostMaxCostFunctionRoutes_ = new unsigned short[spaceWidth_*spaceHeight_];
	if (!hostMaxCostFunctionRelStatesIndexes_)
		hostMaxCostFunctionRelStatesIndexes_ = new RelStateIndexValue[spaceWidth_*spaceHeight_];
	return error;
}

cudaError CudaTrackingBarnivAlgorithm::performBarnivDpaTracking(float* devSpaces[NUMBER_OF_SPACES]) {
	uint blockDimSize = 32;
	uint gridDimX = spaceWidth_ / blockDimSize + 1;
	uint gridDimY = spaceHeight_ / blockDimSize + 1;
	dim3 gridSize(gridDimX, gridDimY);
	dim3 blockSize(blockDimSize, blockDimSize);
	//find top right neighbour of first state
	RelStateIndexValue initTopRightNeighbour = 0;
	RelStateValue xRelState = xRelStates_[0];
	RelStateValue yRelState = yRelStates_[0];
	while (((yRelStates_[initTopRightNeighbour] == yRelState + 1 && xRelStates_[initTopRightNeighbour] < xRelState + 1) ||
		yRelStates_[initTopRightNeighbour] < yRelState + 1) &&
		//topNeighbour < statesCount + 2)
		initTopRightNeighbour < relStatesCount_)
		++initTopRightNeighbour;

	performBarnivDpaTrackingStep << <gridSize, blockSize >> > (devCurrentCostFunction_, devPreviousCostFunction_,
		devSpaces[0], spaceWidth_, spaceHeight_, relStatesCount_,
		0, initTopRightNeighbour, devRoutes_);
	float* temp = nullptr;
	for (int i = 1; i < 5/*NUMBER_OF_SPACES*/; ++i) {
		temp = devCurrentCostFunction_;
		devCurrentCostFunction_ = devPreviousCostFunction_;
		devPreviousCostFunction_ = temp;
		performBarnivDpaTrackingStep << <gridSize, blockSize >> > (devCurrentCostFunction_, devPreviousCostFunction_,
			devSpaces[i], spaceWidth_, spaceHeight_, relStatesCount_,
			i, initTopRightNeighbour, devRoutes_);
	}
	performCostFunctionMaxValuesSearch << < gridSize, blockSize >> > (devCurrentCostFunction_, spaceWidth_, spaceHeight_, relStatesCount_,
		devRoutes_, devMaxCostFunctionValues_, devMaxCostFunctionRoutes_, devMaxCostFunctionRelStatesIndexes_);
	cudaError error = cudaSuccess;
	RETURN_ON_CUDA_ERROR(cudaGetLastError());
	//copy objects to host
	size_t spaceSize = spaceWidth_*spaceHeight_;
	RETURN_ON_CUDA_ERROR(cudaMemcpy(hostMaxCostFunctionValues_, devMaxCostFunctionValues_, spaceSize * sizeof(float), cudaMemcpyDeviceToHost));
	RETURN_ON_CUDA_ERROR(cudaMemcpy(hostMaxCostFunctionRoutes_, devMaxCostFunctionRoutes_, spaceSize * sizeof(unsigned short), cudaMemcpyDeviceToHost));
	RETURN_ON_CUDA_ERROR(cudaMemcpy(hostMaxCostFunctionRelStatesIndexes_, devMaxCostFunctionRelStatesIndexes_, spaceSize * sizeof(RelStateIndexValue), cudaMemcpyDeviceToHost));
	extractRoutes();
	return cudaSuccess;
}