#pragma once

bool initDevice();
bool allocateMemoryForDevice();
bool performCudaTest(unsigned int* mappedPtr, unsigned int width, unsigned int height);
bool cleanupDevice();