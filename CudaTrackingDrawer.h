#pragma once

#define GRADIENT_COLORS_COUNT_ 7

struct DrawingState {
	DrawingState()
		:mappedPboPtr_(nullptr),
		pboWidth_(0),
		pboHeight_(0),
		xOffset_(0),
		yOffset_(0),
		zoom_(1.f),
		minSignal_(-20.f),
		maxSignal_(20.f),
		colorGradient_() {
		colorGradient_[0] = 0xFF00FF00; //green
		colorGradient_[1] = 0xFF00FFFF; //yellow
		colorGradient_[2] = 0xFF0000FF; //red
		colorGradient_[3] = 0x00000000; //3 colors by default
	}
	unsigned int* mappedPboPtr_;	//pointer mapped to pixel buffer object
	unsigned int pboWidth_;			//pixel buffer object width
	unsigned int pboHeight_;		//pixel buffer object height
	int xOffset_;			//object offset in X axis
	int yOffset_;			//object offset in Y axis
	float zoom_;			//object zoom
	float minSignal_;		//min value that has color representation in color map
	float maxSignal_;		//max value that has color representation in color map
	unsigned int colorGradient_[GRADIENT_COLORS_COUNT_]; //gradient that will be used to draw object(value 0x00000000 at index i means only first i values will be used)
};

class CudaTrackingDrawer {
public:
	void drawObject(const float* devObject, const unsigned int objectWidth, const unsigned int objectHeight, const DrawingState& state);
};