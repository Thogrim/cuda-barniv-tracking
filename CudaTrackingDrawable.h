#pragma once

struct DrawingState;

class CudaTrackingDrawable {
public:
	virtual void draw(const DrawingState& drawState) const= 0;
};